
function slideshow(arr){
	$(document).ready(function(){
	//khai báo biến n lưu vị trí ảnh
	n = 0;
	//thực hiện hàm interVal để lặp lại sau 2 giây
	var idslide = setInterval(function(){
		//ẩn ảnh hiện tải, hiển thị ảnh kế tiếp
		$("#slide").fadeOut(function(){
			//tác động vào attribute src của ảnh, thay đổi đường dẫn theo hiệu ứng fadeIn
			$("#slide").attr("src",arr[n]);
			$("#slide").fadeIn();
			n++;
		});
		//nếu n ở vị trí key cuối cùng, reset n = 0
		if(n == arr.length)
			n = 0;
	},3000);
	});
}

