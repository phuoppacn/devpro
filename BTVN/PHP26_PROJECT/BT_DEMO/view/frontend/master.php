<!DOCTYPE html>
<head>
<meta charset="utf-8">
<script type="text/javascript" src="public/frontend/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="public/frontend/js/slide.js"></script>
<script type="text/javascript" src="public/frontend/js/jquery.sticky-kit.js"></script>
<link href="public/frontend/css/style.css" type="text/css" rel="stylesheet" />

<script type="text/javascript">
	//khai báo array chứa danh sách link ảnh
	var arr = new Array();
	arr[0] = "public/frontend/images/toystory.jpg";
	arr[1] = "public/frontend/images/up.jpg";
	arr[2] = "public/frontend/images/walle.jpg";
	//gọi hàm slide thực hiện hiệu ứng
	slideshow(arr);
</script>
<title>PHP - testpage</title>
</head>
<body>
	<div id="wrapper">
	<!-- header -->
	<div id="header">
		<div style="text-align:right; padding-top:50px; padding-right:300px; font-size:20px;">Họ và tên: Phú Nguyễn Văn</div>
	</div>
	<nav class="menu">
		<ul>
			<li><a href="index.php">Trang chủ</a></li>
			<li><a href="#">Giới thiệu</a></li>
			<li><a href="#">Liên hệ</a></li>
		</ul>
	</nav>
	<div id="menu_line"></div>
	<!-- end header -->
	<!-- content -->
	<div id="content">
	<!-- left -->
		<aside class="sticky-left">
			<!-- category -->
			<?php include "controller/frontend/controller_category_news.php" ?>
			<!-- end category -->
			<!-- static -->
			<div id="online">
				<div id="online_text">
				<b>Số lượng truy cập:</b> 123<br />
				<b>Đang online:</b> 123
				</div>
			</div>
			<!-- end static -->
			<!-- support online -->
			<div id="online">
				<div style="padding:5px 0px 0px 0px; font-weight: bold">
				Hỗ trợ bán hàng
				</div>
				<div style="padding:5px 5px 5px 5px;">
				01.2345678
				</div>
				<div style="padding:5px 5px 0px 5px; font-weight: bold;">
				Hỗ trợ kỹ thuật
				</div>
				<div style="padding:5px 5px 5px 5px;">
				01.2345678
				</div>
			</div>
			<!-- end support online -->
			<!-- logo -->
			<div id="logo">
				<div style="text-align:right; font-weight:bold; padding-right:60px; padding-top:6px;">Logo</div>
				<ul>
					<li><a href="#"><img src="public/frontend/images/logo1.jpg" border="0" /></a></li>
					<li><a href="#"><img src="public/frontend/images/adv.png" border="0" /></a></li>
				</ul>
			</div>
			<!-- end logo -->
		</aside>
	<!-- end left -->
	<!-- main -->
		<div id="main">	
		<!-- slideshow -->
		<div class="slideshow">
			<img src="public/frontend/images/walle.jpg" id="slide">
		</div>
		<!-- end slideshow -->
		<!-- list category -->
		<!-- VIEW_HOME -->
		 <?php 
        //lay bien controller tu url
        $controller = isset($_GET["controller"]) ? $_GET["controller"]:"";
        //kiểm tra, nếu $controller có giá trị và tồn tại controller đó
        if($controller != "" && file_exists("controller/frontend/controller_$controller.php")){
          //ghi cac thanh phan de thanh duong dan vat ly
          $controller = "controller/frontend/controller_$controller.php";        
        }else
          $controller = "controller/frontend/controller_home.php";
        if(file_exists($controller))
          include $controller;

     ?>
		<!-- END VIEW_HOME -->
		<!-- end list category -->
		</div>
	<!-- end main -->
	<!-- right -->
		<aside class="sticky-right">
		<!-- hot news -->
			<?php include "controller/frontend/controller_hotnews.php"; ?>
		<!-- end hot news -->
		<!-- logo -->
			<div id="logo">
				<div style="text-align:right; font-weight:bold; padding-right:60px; padding-top:6px;">Logo</div>
				<ul>
					<li><a href="#"><img src="public/frontend/images/logo1.jpg" border="0" /></a></li>
					<li><a href="#"><img src="public/frontend/images/logo2.jpg" border="0" /></a></li>
					<li><a href="#"><img src="public/frontend/images/logo3.jpg" border="0" /></a></li>
				</ul>
			</div>
		<!-- end logo -->
		</aside>
	 <!-- end right -->	
	</div>
	<!-- footer -->
	<div id="footer">
		<br />
		Địa chỉ:Phố Nhổn- Minh Khai- Hà Nội. Email: phunv@gmail.com
	</div>
	<!-- end footer -->
	</div>
</body>


<script type="text/javascript">
    $(document).ready(function(){
        $(".sticky-right").stick_in_parent({
            offset_top: 1
        });
    });
</script>
</html>