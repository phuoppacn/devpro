<?php 
	class model{
		//ham lay tat ca cac ban ghi
		public function get_all_record($sql){
			global $db;
			$query = mysqli_query($db,$sql);
			$arr = array();
			while($rows = mysqli_fetch_object($query))
				$arr[] = $rows;
			return $arr;
		}
		//ham lay mot ban ghi
		public function get_a_record($sql){
			global $db;
			$query = mysqli_query($db,$sql);			
			return mysqli_fetch_object($query);
		}
		//ham thuc thi cau truy van
		public function execute($sql){
			global $db;
			$query = mysqli_query($db,$sql);
		}
		//lay tong so ban ghi
		public function get_num_rows($sql){
			global $db;
			$query = mysqli_query($db,$sql);			
			return mysqli_num_rows($query);
		}
	}
 ?>