<?php 
    //lay các bản ghi thuộc danh mục sản phẩm
    $category = $this->model->get_all_record("select * from tbl_category_news order by pk_category_news_id limit 1,2");
    foreach($category as $rows_category)
    {
 ?>
 <?php 
    //kiem tra, neu danh muc co cac bai tin thi moi hien thi, neu khong thi khong hien thi
    //ham get_num_rows tra ve so luong ban ghi
    $check = $this->model->get_num_rows("select pk_news_id, c_name, c_img from tbl_news where fk_category_news_id=".$rows_category->pk_category_news_id." order by pk_news_id desc limit 0,1");
    if($check > 0)
    {
  ?>

<!-- list category -->
		<div id="box-news">		
			<div class="news-category"><?php echo $rows_category->c_name ?></div>
			<div class="block-news">
				<ul>
					<?php 
            //lay 4 tin tuc sau tin tuc dau tien
            $news = $this->model->get_all_record("select * from tbl_news where fk_category_news_id=".$rows_category->pk_category_news_id." order by pk_news_id desc limit 1,3");
            foreach($news as $rows)
            {
         ?>
					<!-- list news -->
					<li>
						<div class="news-title"><a href="danh-muc-tin/<?php echo $rows->pk_news_id; ?>/<?php echo remove_unicode($rows->c_name);  ?>"><?php echo $rows->c_name;  ?></a></div>

						<div class="news-desc">
							<img style="width: 80px; height: 80px;" src="public/upload/news/<?php echo $rows->c_img; ?>"><?php echo $rows->c_description; ?>
						</div>
						<div class="clear"></div>
						<div class="news-detail"><a href="chi-tiet/<?php echo remove_unicode($rows->c_name);  ?>">Xem tiếp</a></div>
						<div class="news-line"></div>
					</li>
					<!-- end list news -->
					<?php } ?>
				</ul>
			</div>	
		  </div>
		       <?php } ?>
    <?php } ?>