-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 23, 2018 at 01:56 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `php25_database`
--

-- --------------------------------------------------------

--
-- Table structure for table `chucdanh`
--

CREATE TABLE `chucdanh` (
  `machucdanh` int(11) NOT NULL,
  `tenchucdanh` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chucdanh`
--

INSERT INTO `chucdanh` (`machucdanh`, `tenchucdanh`) VALUES
(1, 'Giám đốc'),
(2, 'Phó Giám đốc'),
(3, 'Nhân viên');

-- --------------------------------------------------------

--
-- Table structure for table `nhanvien`
--

CREATE TABLE `nhanvien` (
  `manhanvien` int(11) NOT NULL,
  `maphongban` int(11) NOT NULL,
  `machucdanh` int(11) NOT NULL,
  `hovaten` varchar(500) NOT NULL,
  `diachi` varchar(500) NOT NULL,
  `namsinh` date NOT NULL,
  `luong` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nhanvien`
--

INSERT INTO `nhanvien` (`manhanvien`, `maphongban`, `machucdanh`, `hovaten`, `diachi`, `namsinh`, `luong`) VALUES
(1, 1, 1, 'Nguyễn Văn A', 'Hà Nội', '1990-05-08', 6000000),
(2, 1, 2, 'Nguyễn Văn B', 'Huế', '1991-06-03', 7000000),
(3, 2, 3, 'Nguyễn Văn C', 'Cần Thơ', '1992-12-25', 7000000),
(4, 2, 3, 'Nguyễn Văn D', 'Cà Mau', '1992-04-07', 8000000),
(5, 3, 3, 'Nguyễn Văn E', 'Hà Nội', '1993-05-04', 9000000),
(6, 4, 4, 'Nguyễn Văn F', 'Nha Trang', '1994-09-08', 5000000),
(7, 8, 5, 'Nguyễn Văn G', 'Hà Nam', '1995-07-01', 4000000);

-- --------------------------------------------------------

--
-- Table structure for table `phongban`
--

CREATE TABLE `phongban` (
  `maphongban` int(11) NOT NULL,
  `tenphongban` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phongban`
--

INSERT INTO `phongban` (`maphongban`, `tenphongban`) VALUES
(1, 'Phòng kỹ thuật'),
(2, 'Phòng Kinh doanh'),
(3, 'Phòng kế toán'),
(4, 'Phòng Tổ chức');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chucdanh`
--
ALTER TABLE `chucdanh`
  ADD PRIMARY KEY (`machucdanh`);

--
-- Indexes for table `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`manhanvien`);

--
-- Indexes for table `phongban`
--
ALTER TABLE `phongban`
  ADD PRIMARY KEY (`maphongban`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chucdanh`
--
ALTER TABLE `chucdanh`
  MODIFY `machucdanh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `nhanvien`
--
ALTER TABLE `nhanvien`
  MODIFY `manhanvien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `phongban`
--
ALTER TABLE `phongban`
  MODIFY `maphongban` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
