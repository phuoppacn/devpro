<?php 
//load file model
	include_once "model/model.php";
	class controller_nhanvien{
		//khai bao bien $model
		public $model;
		//ham tao
		public function __construct(){
			//tao object cua model va gan vao bien controller
			$this->model = new model();
			//goi ham danh sach ban ghi trong class model, tra ket qua ve 1 bien

			//co the truyen bien a ,b trong danhsach_ban_ghi, chua truy van ma truy van ben model
			$arr= $this -> model->danhsach_ban_ghi("select * from nhanvien");
			include "view/view_nhanvien.php";
		}
	}
	//de class nay hoat dong , can phai khoi tao class
	new controller_nhanvien();
 ?>