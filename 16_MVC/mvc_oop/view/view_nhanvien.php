<fieldset style="width: 800px; margin: auto;">
	<legend>Danh sach nhan vien</legend>
	<table cellpadding="5" border="1" style="border-collapse: collapse;">
		<tr>
			<th>Họ và tên</th>
			<th>Phòng ban</th>
			<th>Chức danh</th>
			<th>Lương</th>
			<th>Ngày sinh</th>
			<th style="width: 100px;"></th>
		</tr>
		<?php 
			foreach ($arr as $rows) {
				# code...
			
		 ?>
		 <tr>
		 	<td><?php echo $rows->hovaten; ?></td>
		 	<td>
		 		<?php 
		 			//goi ham mot_ban_ghi trong model
		 			$phongban= $this->model->mot_ban_ghi("select * from phongban where maphongban=".$rows->maphongban);
		 			echo isset($phongban->tenphongban)?$phongban->tenphongban:"";
		 		 ?>
		 	</td>
		 	<td>
		 		<?php 
		 			//goi ham mot_ban_ghi trong model
		 			$chucdanh= $this->model->mot_ban_ghi("select * from chucdanh where machucdanh=".$rows->machucdanh);
		 			echo isset($chucdanh->tenchucdanh)?$chucdanh->tenchucdanh:"";
		 		 ?>
		 	</td>
		 	<td><?php echo number_format($rows->luong); ?>VND</td>
		 	<td><?php echo $rows->namsinh; ?></td>
		 	<td style="text-align: center;"></td>
		 </tr>
		 <?php } ?>
	</table>
</fieldset>