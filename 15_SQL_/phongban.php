
<?php 

	//Kết nối csdl
	// Khai báo thông số host
$hostname= "localhost";
//khi mua host thì khai báo theo host được cấp
//khai báo thông số truy cập
//username

	$username="root";
	//đây là mặc định của xampp
	$password= "";
	


	//khai báo tên csdl
	$database ="php26_database";
	//kết nối csdl , trả thông tin kết nối về một biến
	// mysqli là kiểu conect mới xu thế
	$db=mysqli_connect($hostname,$username,$password,$database) or die("can not conect database");
	// Lấy từ trong csdl ra phải ép kiểu(như kiểu đang ấm đi ra ngoài rét, mặc áo khác)
	// set charset để hiển thị tiếng việt
	mysqli_set_charset($db,"UTF8");

	//hàm hiển thị danh sách phòng ban , kết quả trả về array
	function danhsach_phongban(){
		//khai bao bien db thanh bien toan cuc
		global $db;
		//truy vaasn csdl , tra ket qua ve mot bien kieu object
		$query = mysqli_query($db,"select * from phongban ");
		//duyet cac ban ghi va tra ve bien array
		$array = array();
		// fetch_array duyet qua tung ban ghi,tra ve array, fetch oject tra ve oject
		while($rows=mysqli_fetch_array($query))
			$array[]= $rows;
		return $array;
	}
	//hàm xóa 1 phòng ban, tham số truyền vào: mã phòng ban

	function xoa_phongban($maphongban){
		global $db;
		mysqli_query($db,"delete phongban where maphongban=$maphongban");
	}

	// --------------------

	//lấy biến action từ url

	$action= isset($_GET["action"])?$_GET["action"]:"";
	switch ($action) {
		case 'delete_phongban':
			# code...
			$maphongban= isset($_GET["maphongban"])?$_GET["maphongban"]:0;
			xoa_phongban($maphongban);
			echo "ok";
			// header("location:phongban.php");
			break;

		
		default:
			# code...
			break;
	}
	// --------------------
	//Hàm hiểu thị danh sách chức danh
	function danhsach_chucdanh(){
		global $db;
		$query= mysqli_query($db,"select * from chucdanh");
		$arr = array();

		while($rows = mysqli_fetch_object($query))
			$arr[] =$rows;
		return $arr;

	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Phong ban</title>
	<meta charset="utf-8">
	<link rel='stylesheet prefetch' href='//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css'>
</head>
<style type="text/css">
	.social {
  text-align: center;
  font-size: 2.5em;
  color: #555;
  overflow: hidden;
}
.social a {
  color: inherit;
  text-decoration: none;
}
.social i {
  margin: .3em;
  cursor: pointer;
  transition: color 300ms ease, margin-top 300ms ease;
  transform: translateZ(0);
}
.social i:hover {
  margin-top: -1px;
}
.social i#twitter:hover {
  color: #77DDF6;
}
.social i#github:hover {
  color: black;
}
.social i#linkedin:hover {
  color: #0177B5;
}
.social i#code:hover {
  color: #29A329;
}
.social i#stack:hover {
  color: #ED780E;
}
.social i#plus:hover {
  color: #D43402;
}
.social i#mail:hover {
  color: #F7B401;
}
</style>
<script src="jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
  $('i').hide();
})
 
$(window).load(function() {
  $('i').show();
 
  var twitterPos = $('#twitter').position();
  var githubPos = $('#github').position();
  var stackPos = $('#stack').position();
  var linkedinPos = $('#linkedin').position();
  var codePos = $('#code').position();
  var plusPos = $('#plus').position();
  var mailPos = $('#mail').position();
  var imgPos = $('.me').position();
   
  $('i').css({
    position: 'absolute',
    zIndex: '1',
    top: imgPos.top + 100,
    left: '47%'
  });
   
  setTimeout(function() {
    $('#twitter').animate({
      top: twitterPos.top + 10,
      left: twitterPos.left - 10
    }, 500);
  }, 250);
   
  setTimeout(function() {
    $('#twitter').animate({
      top: twitterPos.top,
      left: twitterPos.left
    }, 250);
     
    $('#github').animate({
      top: githubPos.top + 10,
      left: githubPos.left - 6
    }, 500);
  }, 500);
   
  setTimeout(function() {
    $('#github').animate({
      top: githubPos.top,
      left: githubPos.left
    }, 250);
     
    $('#stack').animate({
      top: stackPos.top + 10,
      left: stackPos.left - 3
    }, 500);
  }, 750);
   
  setTimeout(function() {
    $('#stack').animate({
      top: stackPos.top,
      left: stackPos.left
    }, 250);
     
    $('#linkedin').animate({
      top: linkedinPos.top + 10,
      left: linkedinPos.left
    }, 500);
  }, 1000);
   
  setTimeout(function() {
    $('#linkedin').animate({
      top: linkedinPos.top,
      left: linkedinPos.left
    }, 250);
     
    $('#code').animate({
      top: codePos.top + 10,
      left: codePos.left + 3
    }, 500);
  }, 1250);
   
  setTimeout(function() {
    $('#code').animate({
      top: codePos.top,
      left: codePos.left
    }, 250);
     
    $('#plus').animate({
      top: plusPos.top + 10,
      left: plusPos.left + 6
    }, 500);
  }, 1500);
   
  setTimeout(function() {
    $('#plus').animate({
      top: plusPos.top,
      left: plusPos.left
    }, 250);
     
    $('#mail').animate({
      top: mailPos.top + 10,
      left: mailPos.left + 10
    }, 500);
  }, 1750);
   
  setTimeout(function() {
    $('#mail').animate({
      top: mailPos.top,
      left: mailPos.left
    }, 250);
  }, 2000);
   
})
//# sourceURL=pen.js
</script>   
<body>
	<fieldset style="width: 400px; margin: auto;">
		<legend>Danh sach phong ban</legend>
		<table cellpadding="5" border="1" style="width: 100%; border-collapse: collapse;">
			<tr>
				<th>ma phong ban</th>
				<th>Ten phong ban</th>
				<th style="width: 100px;"></th>
			</tr>
			<?php 
			//goi ham danh sach phong ban
				$phongban= danhsach_phongban();
				foreach ($phongban as $rows) {
					# code...
				// echo "<pre>";
				// print_r($phongban);
				// echo "<pre>";

			 ?>
			<tr>
				<td> <?php echo $rows["maphongban"]; ?></td>
				<td> <?php echo $rows["tenphongban"]; ?></td>
				<td style="text-align: center;">
					<a href="phongban.php?action=delete_phongban&maphongban=<?php echo $rows["maphongban"]; ?>">Delete</a>
				</td>
			</tr>

			<?php } ?>
		</table>

	</fieldset>

	<fieldset style="width: 400px; margin: auto;">
		<legend>Danh sach phong ban</legend>
		<table cellpadding="5" border="1" style="width: 100%; border-collapse: collapse;">
			<tr>
				<th>ma phong ban</th>
				<th>Ten chuc danh</th>
				<th style="width: 100px;"></th>
			</tr>
			<?php 
			//goi ham danh sach phong bsan
				$chucdanh= danhsach_chucdanh();
				foreach ($chucdanh as $rows) {
					# code...
				//KHAC BIET GIUA OBJECT VA ARRAY chi o luc xuat bien, dung cai nao cung duoc, bay gio thuong sd oj
			 ?>
			<tr>
				<td></td>
				<td> <?php echo $rows->tenchucdanh; ?></td>
				<td style="text-align: center;"></td>
			</tr>

			<?php } ?>

		</table>
	</fieldset>

	<div class="social">
  <a href="#"><i id="twitter" class="icon-twitter"></i></a>
  <i id="github" class="icon-github"></i>
  <i id="stack" class="icon-stackexchange"></i>
  <i id="linkedin" class="icon-linkedin-sign"></i>
  <i id="code" class="icon-code"></i>
  <i id="plus" class="icon-google-plus-sign"></i>
  <i id="mail" class="icon-envelope"></i> 
</div>
</body>
</html>