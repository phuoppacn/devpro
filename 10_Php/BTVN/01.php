<!DOCTYPE html>
<html>
<head>
	<title>01</title>
	<meta charset="utf-8">

</head>
<style type="text/css">
	.wrap{
		width: 500px;
		height: 300px;
		background: #ddd;
		margin: 0 auto;
		
	}
	header{
		text-align: center;
		background: #ddd333;
		margin-bottom: 30px;
		padding: 4px;

	}
	.text{
		float: left;
		padding-right: 30px;
		padding-left: 30px;
	}
	.text li{
		list-style: none;
	}
	.input{

	}
	.input li{
		list-style: none;
	}
	.input li:last-child{
		margin: 0 auto;
		text-align: center;
		padding-top: 20px;
	}
	.button{
		margin: 0 auto;
		border-radius: 5px;
		padding: 2px;
		background: #dd5555;
	}
	footer p:first-child{
		color: red;
		text-align: center;
		background: white;
		padding: 5px;
	}
	footer p:last-child{
		
		text-align: center;
		background: white;
		padding: 5px;
	}
</style>

<body>


	<div class="wrap">
		<header>Màu chữ - Màu nền</header>
	<div class="body">
		<div class="text">
			<li>Nội dung:</li>
			<li>Màu nền:</li>
			<li>Màu chữ:</li>
		</div>
		<div class="input">
			<form method="post" action="">
				<li><input placeholder="Phú Oppa" type="text" name="noidung" style="width: 300px;"></li>
				<li><input placeholder="#666" type="text" name="maunen"></li>
				<li><input placeholder="#555" type="text" name="mauchu"></li>
				<li><input class="button" type="submit" name="xemketqua" value="Xem kết quả" style=""></li>
			</form>
		</div>

	</div>
	<footer>
		<p>Kết quả sau khi nhấn Xem kết quả</p>
		<p style="background: <?php echo($_POST["maunen"]) ?>; color:<?php echo($_POST["mauchu"]) ?>;"><?php echo($_POST["noidung"]) ?></p>
	</footer>
	</div>
</body>
</html>

<!-- My experience

- Khi click button su lieu se duoc gui len server
- Sau do nếu muốn lấy dữ liệu : phải gán dữ liệu lấy từ server vào biến tạm . $noidung=$_POST["noidung"];
$_POST["noidung"]; : lay dữ liệu theo kiểu post từ thẻ input có name= "noidung";
 -->