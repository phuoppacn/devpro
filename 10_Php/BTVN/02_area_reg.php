<!DOCTYPE html>
<html>
<head>
	<title>02_Calculate the area of Rectangle</title>
	<meta charset="utf-8">
</head>
<style type="text/css">
	
	.wrap{
		height: 200px;
		width: 300px;
		background: white;
		margin: 0 auto;
		border: 1px solid #ddd;
	}
	.wrap li{
		list-style: none;
	}
	.wrap p{
		padding: 4px;
		margin-top: 0px;
		text-align: center;
		color: white;
		background: orange;
	}
	.hcn{
		float: left;
		margin: 20px;
	}
	.ht{
		float: left;
		margin: 20px;
	}

	.text{
			float: left;
			padding-left: 10px;
			padding-right: 10px;

		}
	.wrap .input li:last-child{
		text-align: center;
		padding: 20px;
	}
	.button{
		margin: 5px 5px 5px 0;
		font-size: 16px;
		color: white !important;
		background-color: #4CAF50 !important;
		border: none;
		text-decoration: none;
		padding: 8px 16px;
		cursor: pointer;
		text-align: center;
		overflow: hidden;
		white-space: nowrap;
	}
	.button:hover{
		box-shadow: 0 4px 10px 0 rgba(0,0,0,0.2), 0 4px 20px 0 rgba(0,0,0,0.19);
	}
</style>
<body>

<div class="wrap hcn">
	
		<p>Diện tích hình chữ nhật</p>
	<div class="text">
		<li>Chiều dài:</li>
		<li>Chiều rộng:</li>
		<li>Diện tích:</li>
	</div>

	<div class="input">
		<?php 

		$chieudai=isset($_POST["chieudai"])?$_POST["chieudai"]:0;
		$chieurong=isset($_POST["chieurong"])?$_POST["chieurong"]:0;

		$dientich=$chieurong*$chieudai;
		 ?>
		<form action="02_area_reg.php" method="post">
			<li><input type="text" name="chieudai" required="" placeholder="20" value="<?php echo(isset($chieudai)?$chieudai:"") ?>"></li>
			<li><input type="text" name="chieurong" required="" placeholder="10" value="<?php echo(isset($chieurong)?$chieurong:"") ?>"></li>
			<li><input type="text" name="dientich" readonly="" placeholder="200" value="<?php echo(isset($dientich)?$dientich:"") ?>"></li>
			<li><input type="submit" class="button" value="Tính"></li>
		</form>
	</div>
	<div style="clear: both;"></div>
</div>
<div class="wrap ht">
	<p>Diện tích và Chu vi hình tròn</p>
	<div class="text">
		<li>Bán kính:</li>
		<li>Diện tích:</li>
		<li>Chu vi:</li>
	</div>

	<div class="input">
		<?php 

		$bankinh=isset($_POST["bankinh"])?$_POST["bankinh"]:0;
		$chuvi=2*$bankinh*3.14;
		$dientich=2*$bankinh*$bankinh*3.14;
		 ?>
		<form action="02_area_reg.php" method="post">
			<li><input type="text" name="bankinh" required="" placeholder="20" value="<?php echo(isset($bankinh)?$bankinh:"") ?>"></li>
			<li><input type="text" name="chuvi" readonly=""  placeholder="10" value="<?php echo(isset($chuvi)?$chuvi:"") ?>"></li>
			<li><input type="text" name="dientich" readonly="" placeholder="200" value="<?php echo(isset($dientich)?$dientich:"") ?>"></li>
			<li><input type="submit" class="button" value="Tính"></li>
		</form>
	</div>
	<div style="clear: both;"></div>
</div>
</body>
</html>