<!DOCTYPE html>
<html>
<head>
	<title>PHP</title>
	<meta charset="utf-8">

</head>

<!-- 
		Form
			-sử dụng để gửi dữ kiệu trên trang hoặc từ trang này qua trang khác
			- các phương thức của form
				- GET: truyền dữ liệu lên url
				- POST: truyền dữm liệu ngầm

			-Đề lấy dữ liệu theo kiểu post: sử dụng đối tượng $_POST["ten formcontrol"]
			-Đề lấy dữ liệu theo kiểu GET: sử dụng đối tượng $_GET["ten formcontrol"]

			-Một số thuộc tính của thẻ form
				method =post/get
				action ="ten trang" _> dịa chỉ form sẽ submit đến

			-Đối tượng $_SERVER["REQUEST_METHOD"]	 sẽ hiển thị trạng thái của form
				-Trang luôn ở trạng thái GET, khi ấn nút submit thì trang sẽ ở trạng thái POST
				-Hàm isset (doituong) sẽ trả về true nếu đối tượng đó tồn tại, trả về false nếu đối tượng đó không tồn tại
-->
<body>
<?php
	//neu form ở trạng thái post
	$background ="";
	if($_SERVER["REQUEST_METHOD"] == "POST"){
	$mausac =isset($_POST["color"]) ? $_POST["color"] : "";

	switch($mausac){
		case "xanh":
			$background= "background-color: green";
			break;
		case "do":
			$background= "background-color: red";
			break;
		
		case "tim":
			$background= "background-color: purple";
			break;
		
		case "vang":
			$background= "background-color: yellow";
			break;
		
		default:
			$background= "background-color: black";
			break;
		
}
}
	
?>
<style type="text/css">
	.box{
		width: 200px; height: 200px; border: 1px solid #ddd; margin: 15px auto;
		<?php echo $background;  ?>
	}
</style>
	<fieldset style="width: 300px; margin: auto;">
		<legend>Form post</legend>
		<!-- //Mặc định là get -->

		<form method="post" action="hello.php">
			<!-- co thể cho action= "" khi không muốn action đi trang khác, mặc định tự load lại hello.php -->

			<input type="radio" <?php echo  isset($mausac) && $mausac=="xanh" ? "checked" : ""; ?>  name="color" value="xanh"> Xanh
			<input type="radio" <?php echo  isset($mausac) && $mausac=="do" ? "checked" : ""; ?> name="color" value="do">Đỏ
			<input type="radio" <?php echo  isset($mausac) && $mausac=="do" ? "checked" : ""; ?>  name="color" value="tim"> Tím
			<input type="radio" <?php echo  isset($mausac) && $mausac=="do" ? "checked" : ""; ?>  name="color" value="vang"> Vàng
			<input type="submit" value="Chọn">
			<h1 class="box"></h1>
			
		</form>
	</fieldset>
</body>
</html>