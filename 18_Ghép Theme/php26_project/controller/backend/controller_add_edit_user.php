<?php 
	class controller_add_edit_user{
		public $model;
		public function __construct(){
			$this->model = new model();
			//--------
			$id = isset($_GET["id"])?$_GET["id"]:0;
			$act = isset($_GET["act"])?$_GET["act"]:"";
			switch($act){
				case "edit":
					$form_action = "admin.php?controller=add_edit_user&act=do_edit&id=$id";
					//lay mot ban ghi
					$arr = $this->model->get_a_record("select * from tbl_user where pk_user_id=$id");
					//load view
					include "view/backend/view_add_edit_user.php";
				break;
				case "do_edit":
					$c_fullname = $_POST["c_fullname"];
					$c_email = $_POST["c_email"];
					$c_password = $_POST["c_password"];
					$c_password= md5($c_password);
					
					$fk_category_user_id = $_POST["fk_category_user_id"];
					//edit ban ghi
					$this->model->execute("update tbl_user set c_fullname='$c_fullname',c_email='$c_email',c_password='$c_password' where pk_user_id=$id");					
					//upload image
					$c_img = "";
					if($_FILES["c_img"]["name"] != ""){
						//-----------
						//kiem tra, neu co anh cu thi xoa anh do di
						//lay anh o truong c_img trong table tbl_user ung voi id truyen vao
						$old_img = $this->model->get_a_record("select c_img from tbl_user where pk_user_id=$id");
						//kiem tra, neu file ton tai thi xoa file nay
						if(file_exists("public/upload/user/".$old_img->c_img)&&$old_img->c_img!=""){
							//xoa anh bang ham unlink
							unlink("public/upload/user/".$old_img->c_img);
						}
						//-----------
						$c_img = time().$_FILES["c_img"]["name"];
						//upload file
						move_uploaded_file($_FILES["c_img"]["tmp_name"], "public/upload/user/$c_img");
						//update truong c_img cua ban ghi co id truyen vao
						$this->model->execute("update tbl_user set c_img='$c_img' where pk_user_id=$id");
					}
					//------------
					//quay lai trang tin tuc
					header("location:admin.php?controller=user");
				break;
				case "add":
					//dinh nghia bien $form_action de chi action cua form
					$form_action = "admin.php?controller=add_edit_user&act=do_add";
					//load view
					include "view/backend/view_add_edit_user.php";
				break;
				case "do_add":
					$c_fullname = $_POST["c_fullname"];
					$c_email = $_POST["c_email"];
					$c_password = $_POST["c_password"];
					
					
					//upload image
					$c_img = "";
					if($_FILES["c_img"]["name"] != ""){
						$c_img = time().$_FILES["c_img"]["name"];
						//upload file
						move_uploaded_file($_FILES["c_img"]["tmp_name"], "public/upload/user/$c_img");
					}
					//them ban ghi vao csdl
					$this->model->execute("insert into tbl_user(c_fullname,c_email,c_password) values('$c_fullname','$c_email','$c_password')");
					//------------
					//quay lai trang tin tuc
					header("location:admin.php?controller=user");
				break;
			}
			//--------
		}
	}
	new controller_add_edit_user();
 ?>