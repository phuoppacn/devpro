<?php 
	include 'config.php';
	class model{
		//hàm lấy tất cả các bản ghi
		public function get_all_record($sql){
			global $db;
			$query = $db->query($sql);
			$arr = array();
			while ($row = mysqli_fetch_object($query)){
				$arr[] = $row;
			}
			return $arr;
		}
		//lấy một bản ghi
		public function get_a_record($sql){
			global $db;
			$query = $db->query($sql);
			return mysqli_fetch_object($query);
		}
		//hàm thực thi câu truy vấn
		public function execure($sql){
			global $db;
			$db->query($sql);
		}
		//hàm lấy tổng số bản ghi
		public function get_num_rows($sql){
			global $db;
			$query = $db->query($sql);
			return mysqli_num_rows($query);
		}
	}
?>