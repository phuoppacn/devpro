<?php 
	class controller_product{
		//tạo biến $model
		public $model;
		public function __construct(){
			$this->model = new model();
			//------
			$atc = isset($_GET["act"]) ? $_GET["act"] : "";
			switch ($atc) {
				case 'delete':
					$id = isset($_GET["id"]) ? $_GET["id"] : "";
					//kiểm tra nếu có ảnh cũ thì xóa ảnh đó đi
					//lấy ảnh ở trường c_img trong table tbl_product ứng với id truyền vào
					$old_img = $this->model->get_a_record("select c_img from tbl_product where pk_product_id=$id");
					if(file_exists("public/upload/product/".$old_img->c_img)&&$old_img->c_img!=''){
						//xóa ảnh bằng hàm unlink
						unlink("public/upload/product/".$old_img->c_img);
					}
					//thực hiện xóa ảnh
					$this->model->execure("delete from tbl_product where pk_product_id=$id");
					header("location:admin.php?controller=product");
					break;
			}
			//số bản ghi trên 1 trang 
			$record_per_page = 10;
			//tính tổng số bản ghi trong table
			$total = $this->model->get_num_rows("select c_name from tbl_product");
			//số trang = tổng số bản ghi chia(/) cho số bản ghi trên 1 trang
			$num_page =ceil($total / $record_per_page);
			//lấy trang hiện tại (biến này truyền trên url)
			$page = isset($_GET["p"])&&$_GET["p"]>0 ? ($_GET["p"] - 1) : 0; 
			//từ trang hiện tại xác định lấy từ bản ghi nào
			$from = $page * $record_per_page;
			//---------
			$arr = $this->model->get_all_record("select * from tbl_product order by pk_product_id desc limit $from,$record_per_page");
			//load view
			include 'view/backend/view_product.php';
			//---------
		}
	}
	new controller_product();
?>