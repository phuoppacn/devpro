<?php 
	class controller_add_edit_product{
		public $model;
		public function __construct(){
			$this->model = new model();
			//-------
			$id = isset($_GET["id"]) ? $_GET["id"] : "";
			$action = isset($_GET["act"]) ? $_GET["act"] : "";
			switch ($action) {
				case 'edit':
					$form_action = "admin.php?controller=add_edit_product&act=do_edit&id=$id";
					//lấy một bản ghi
					$arr = $this->model->get_a_record("select * from tbl_product where pk_product_id=$id");
					include 'view/backend/view_add_edit_product.php';
					break;
				case 'do_edit':
					$c_name = $_POST["c_name"];
					$c_description = $_POST["c_description"];
					$c_content = $_POST["c_content"];
					$c_hotproduct = isset($_POST["c_hotproduct"]) ? 1 : 0;
					$fk_category_product_id = $_POST["fk_category_product_id"];
					//edit
					$this->model->execure("update tbl_product set c_name='$c_name',c_description='$c_description',c_content='$c_content',c_hotproduct=$c_hotproduct,fk_category_product_id='$fk_category_product_id' where pk_product_id=$id");

					//upload ảnh
					$c_img = "";
					if($_FILES["c_img"]["name"] != ""){
						//kiểm tra nếu có ảnh cũ thì xóa ảnh đó đi
						//lấy ảnh ở trường c_img trong table tbl_product ứng với id truyền vào
						$old_img = $this->model->get_a_record("select c_img from tbl_product where pk_product_id=$id");
						if(file_exists("public/upload/product/".$old_img->c_img)&&$old_img->c_img!=''){
							//xóa ảnh bằng hàm unlink
							unlink("public/upload/product/".$old_img->c_img);
						}
						$c_img = time().$_FILES["c_img"]["name"];
						//upload file
						move_uploaded_file($_FILES["c_img"]["tmp_name"], "public/upload/product/$c_img");
						//upload trường c_img của bản ghi có id truyền vào
						$this->model->execure("update tbl_product set c_img='$c_img' where pk_product_id=$id");
					}
					//------------
					header("location:admin.php?controller=product");
					break;
				case 'add':
					//Định nghĩa biến form_action để chỉ action của form
					$form_action = "admin.php?controller=add_edit_product&act=do_add";
					//load view
					include 'view/backend/view_add_edit_product.php';
					break;
				case 'do_add':
					$c_name = $_POST["c_name"];
					$c_description = $_POST["c_description"];
					$c_content = $_POST["c_content"];
					$c_hotproduct = isset($_POST["c_hotproduct"]) ? 1 : 0;
					//upload ảnh
					$c_img = "";
					if($_FILES["c_img"]["name"] != ""){
						$c_img = time().$_FILES["c_img"]["name"];
						//upload file
						move_uploaded_file($_FILES["c_img"]["tmp_name"], "public/upload/product/$c_img");
					}
					//thêm bản ghi
					$this->model->execure("insert into tbl_product (c_name,c_description,c_content,c_img,c_hotproduct) values ('$c_name','$c_description','$c_content','$c_img',$c_hotproduct)");
					//------------
					header("location:admin.php?controller=product");
					break;
			}
			//--------
		}
	}
	new controller_add_edit_product();
?>