<?php 
	class controller_login{
		//khai báo biến model
		public $model;
		//hàm tạo
		public function __construct(){
			//khởi tạo class model, gán vào biến $model của class này để từ đó có thể truy cập vào các hàm của class model
			$this->model = new model();
			//-------
			if($_SERVER["REQUEST_METHOD"] == "POST"){
				$c_email = $_POST["c_email"];
				$c_password = $_POST["c_password"];
				if($this->check_login($c_email,$c_password) == true){
					header("location:admin.php");
				}else{
					header("location:admin.php?err=fail");
				}
			}
			//--------
			//load view
			include 'view/backend/view_login.php';
		}
		public function check_login($c_email,$c_password){
			//kiểm tra xem email có tồn tại trong csdl hay k
			$check = $this->model->get_a_record("select c_email,c_password from tbl_user where c_email='$c_email'");
			if(isset($check->c_email)){
				//kiểm tra password
				$c_password = md5($c_password);
				if($c_password == $check->c_password){
					//đăng nhập thành công
					$_SESSION["c_email"] = $c_email;
					return true;
				}
			}
			return false;
		}
	}
	new controller_login();
?>