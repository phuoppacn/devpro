<?php 
	class controller_news_category{
		//tạo biến $model
		public $model;
		public function __construct(){
			$this->model = new model();
			//------
			$category_id=isset($_GET["category_id"]) && is_numeric($_GET["category_id"])?$_GET["category_id"]:0;
			//số bản ghi trên 1 trang 
			$record_per_page = 4;
			//tính tổng số bản ghi trong table
			$total = $this->model->get_num_rows("select c_name from tbl_news");
			//số trang = tổng số bản ghi chia(/) cho số bản ghi trên 1 trang
			$num_page =ceil($total / $record_per_page);
			//lấy trang hiện tại (biến này truyền trên url)
			$page = isset($_GET["p"])&&$_GET["p"]>0 ? ($_GET["p"] - 1) : 0; 
			//từ trang hiện tại xác định lấy từ bản ghi nào
			$from = $page * $record_per_page;
			//---------
			$arr = $this->model->get_all_record("select * from tbl_news where fk_category_news_id=$category_id order by pk_news_id desc limit $from,$record_per_page");
			//load view
			include 'view/frontend/view_news_category.php';
			//---------
		}
	}
	new controller_news_category();
?>