<div class="row justify-content-center">
	<div class="col-md-12">
		<!-- card -->
		<div style="margin:15px 0px">
			<a href="admin.php?controller=add_edit_product&act=add" class="btn btn-primary">Add</a>
		</div>
		<div class="card border-primary">
			<div class="card card-header bg-primary text-white" style="padding:7px !important;">Login</div>
				<div class="card-body">
				<!-- table -->
				<table class="table table-hover table-bordered">
					<tr>
						<th style="width: 150px;">Ảnh</th>
						<th>Tiêu đề</th>
						<th style="width: 150px;">Danh mục</th>
						<th style="width: 120px;">Tin nổi bật</th>
						<th style="width: 150px;"></th>
					</tr>
					<?php 
						foreach ($arr as $key => $value) {
							
					?>
					<tr>
						<td>
							<?php 
								if(file_exists("public/upload/product/$value->c_img")){ 
							 ?>
							 		<img style="width: 150px;" src="public/upload/product/<?php echo $value->c_img; ?>">
							 <?php } ?>
						 </td>
						<td><?php echo $value->c_name; ?></td>
						<td><?php 
						//lấy một bản ghi
							$categoty = $this->model->get_a_record("select c_name from tbl_category_product where pk_Category_product_id=".$value->fk_category_product_id);
							echo isset($categoty->c_name) ? $categoty->c_name : "";
						?></td>
						<td style="text-align: center;">
							<?php 
								if($value->c_hotproduct == 1){ ?>
									Hot
							<?php } ?>
						</td>
						<td style="text-align: center;">
							<a class="btn btn-warning" href="admin.php?controller=add_edit_product&act=edit&id=<?php echo $value->pk_product_id ?>">Edit</a>&nbsp;&nbsp;
							<a onclick="return window.confirm('Are you sure?');" class="btn btn-danger" href="admin.php?controller=product&act=delete&id=<?php echo $value->pk_product_id ?>">Delete</a>
						</td>
					</tr>
					<?php } ?>
				</table>
				<!-- end table -->
				</div>
				<div class="card-footer" style="padding:7px !important">
					<ul class="pagination">
						<li class="page-item"><a class="page-link" href="#">Trang</a></li>
						<?php 
							for($i = 1; $i <= $num_page; $i++){
						?>
						<li class="page-item"><a class="page-link" href="admin.php?controller=product&p=<?php echo $i; ?>"><?php echo $i; ?></a></li>
						<?php } ?>
					</ul>		
				</div>
			</div>
		</div>
		<!-- end card -->
	</div>
</div>