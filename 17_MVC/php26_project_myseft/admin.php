<?php 
	session_start();
	//file hiển thị backend
	//load file model
	include 'model/model.php'; 
	//kiểm tra nếu user chưa đăng nhập thì hiển thị MVC login, nếu user đã đăng nhập thì hiển thị file master.php
	if(isset($_SESSION["c_email"]) == false){
		include "controller/backend/controller_login.php";
	}else{
		//user đã đăng nhập
		include "view/backend/master.php";
	}
?>