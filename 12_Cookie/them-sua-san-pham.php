<?php 
	// start session
	session_start();
	// kiểm tra biến session product, nếu không tồn tại thì khởi tạo nó

	if (isset($_SESSION["product"])==false) {
		# code...
		$_SESSION["product"]= array();
	}
 ?>
<!--  -->
<!DOCTYPE html>
<html>
<head>
	<title>Thêm sửa sản phẩm</title>
	<meta charset="utf-8">
</head>
<body>
	
</body>

	<!-- <?php 
		// session_start();
		// echo "<h1>".$_SESSION["tenlop"]."</h1>";
	?> -->

	<div style="width: 1000px; margin: 20px auto;">
		<?php include 'menu.php'; ?>

		<!-- tạo form để thêm sản phẩm vào -->

		<?php 
			// lấy biến action từ url
		$action= isset($_GET["action"])? $_GET["action"]: "";
		// khi bao bien $form_action de luu action của form
		$form_action= "";
		switch ($action) {
			case 'add':
				# code...
				$form_action = "them-sua-san-pham.php?action=do_add";
				break;
			
			case 'do_add':
				# code...
				$ten = $_POST["ten"];
				$gia = $_POST["gia"];
				// -----------

				$anh="";
				// lay ten anh
				// thêm hàm time để cho dù ảnh có tên giống nhau thì vẫn không bị ghi đè
				$anh= time().$_FILES["anh"]["name"];
				// thực hiện upload ảnh
				move_uploaded_file($_FILES["anh"]["tmp_name"], "upload/$anh");

				// thư mục này sẽ không tự tạo, vì vậy phải tạo
				// -----------

				// gán giá trị vào biến session array
				// vì là array lên phải có []
				$_SESSION["product"][] = array('ten' =>$ten ,"gia"=>$gia,"anh"=>$anh );

				// di chuyển đến trang index.php bằng cú pháp sau

				header("location: index1.php");
				break;

			case 'delete':
				# code...
				$key = isset($_GET["key"])?$_GET["key"]:"";

				// duyệt các phần tử của session array , nếu key= bien key truyền vào từ url thì xóa phần tử đó

				foreach ($_SESSION["product"] as $k => $v) {
					# code...

					if ($k == $key) {
						# code...
						// xóa file bằng hàm unlink (duongdan)
						unlink("upload/".$_SESSION["product"][$key]["anh"]);
					
					// xóa phần tử của array bằng hàm unset(tenarray)[key])
					unset($_SESSION["product"][$key]);
					}
				}
				break;
			default:
				# code...
				break;
		}
		 ?>
		<fieldset style="width: 500px; margin: auto;">
			<legend>Thêm sửa sản phảm </legend>

			<!-- Click link "them mới", khi đó sẽ đến trang them-sua-san-pham.php?ac  -->
			<form method="post" enctype="multipart/form-data" action="<?php echo $form_action; ?>">
				<table cellpadding="5">
					<tr>
						<td>Tên sp</td>
						<td><input type="text" required name="ten"></td>

					</tr>
					<tr>
						<td>Giá</td>
						<td><input type="number" min="0" name="gia"></td>
						
					</tr>
					<tr>
						<td>File</td>
						<td><input type="file" name="anh"></td>

					</tr>
					<tr>
						<td></td>
						<td><input type="submit" value="Process"></td>

					</tr>
					
				</table>
			</form>
		</fieldset>
	</div>
</html>