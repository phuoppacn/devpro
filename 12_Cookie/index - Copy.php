<!DOCTYPE html>
<html>
<head>
	<title>Secsion & cookie</title>
	<meta charset="utf-8">
</head>
<body>
	<!-- 
		-Cookie : là đoạn mã lưu ở máy người dùng( phía client)
		- Cookie sẽ lưu theo thông tin sau:
			- Có thời gian tồn tại.
				VD: nếu thời gian tồn tại là 5p, thì sau 5p cookie sẽ tự động hủy
			- Hàm time() sẽ đổi thời gian ra thành số nguyên

			- Để tạo cookie, sử dụng cấu trúc: setcookie(tenbien,giatri,thoigiantontai)
			-Để lấy giá trị của biến cookie, sử dụng cấu trúc: $_COOKIE["tenbien"];

			-Để hủy cookie , chỉ cần đưa thời gian timeout về trước thoigiantontai




		- Session : lưu ở phía server
			- Để thực hiện biến session tồn tại trên trình duyệt, có nghĩa là ở tất cả các tab trong trình duyệt đó
			- Ứng với mỗi trình duyệt, sẽ có một session_id khác nhau. Khi tắt trình duyệt thì session cũng mất đi, có nghĩa là session tồn tại theo trình duyệt
			- Phải start session trước khi sử dụng bằng hàm session_start()
			- session id: session_id()
			-Khai báo biến session : $_SESSION
	 -->
</body>

	<?php 

		//khoi tao bien cookie, thoi gian timeout là 10giay

		//khi mới khỏi tạo biến lần 1 , cookie sẽ được lưu vào máy và chưa thể hiện thị luôn, nhưng sau đó 1 lúc f5 lại sẽ có thể hiển thị, sau 10s f5 lại mất
		setcookie("lop","php26",time() + 10);
		// setcookie("lop","php26",time() - 10); huy
		echo "<h1>".$_COOKIE["lop"]."</h1>";
	?>
</html>