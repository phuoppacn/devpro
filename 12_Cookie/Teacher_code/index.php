<!DOCTYPE html>
<html>
<head>
	<title>Session & Cookie</title>
	<meta charset="utf-8">
</head>
<!-- 
	- Cookie: là đoạn mã lưu ở máy người dùng (phía client)
	- Cookie sẽ lưu theo thông tin sau:
		- Có thời gian tồn tại. VD: nếu thời gian tồn tại là 5 phút, thì sau 5 phút đó cookie sẽ tự huỷ
		- Hàm time() sẽ đổi thời gian hiện tại ra thành 1 số nguyên
		- Để tạo cookie, sử dụng cấu trúc: setcookie(tenbien,giatri,thoigiantontai)
		- Để lấy giá trị của biến cookie, sử dụng cấu trúc: $_COOKIE["tenbien"]
		- Để huỷ cookie, chỉ cần đưa thời gian timeout về trước thoigiantontai
	- Session: lưu ở phía server
		- Biến session tồn tại trên trình duyệt, có nghĩa là ở tất cả các tab trong trình duyệt đó
		- Ứng với mỗi trình duyệt, sẽ có một session_id khác nhau. Khi tắt trình duyệt thì session cũng mất đi, có nghĩa là session tồn tại theo từng trình duyệt
		- Phải start session trước khi sử dụng bằng hàm session_start()
		- session id: session_id()
		- Khai báo biến session: $_SESSION["tenbien"]
	- upload ảnh
		- Trong thẻ form, để upload ảnh bắt buộc phải có thuộc tính enctype="multipart/form-data"
		- Để lấy giá trị của file, dùng đối tượng $_FILES["tenformcontrol"]
			- $_FILES["tenformcontrol"][name] -> tên file
			- $_FILES["tenformcontrol"][size] -> kích thước file, tính bằng kb
			- $_FILES["tenformcontrol"][tmp_name] -> đường dẫn file upload vào thư mục tạm
		- Di chuyển file từ thư mục tạm vào thư mục chỉ định bằng hàm move_uploaded_file(tmp_name,tenthumuc)
 -->
<body>
<?php
	session_start();	
?>
<style type="text/css">
	body{font-family: Arial}
	a{text-decoration: none;}
</style>
<!-- load menu.php -->
<div style="width: 1000px; margin:20px auto;">
	<?php include "menu.php"; ?>
	<fieldset style="width: 600px; margin:30px auto;">
		<legend>Danh sách sản phẩm</legend>
		<table cellpadding="5" border="1" style="width: 100%; border-collapse: collapse;">
			<tr>
				<th style="width: 100px;">Ảnh</th>
				<th>Tên sản phẩm</th>
				<th>Giá</th>
				<th style="width: 100px;"></th>
			</tr>
			<!-- kiem tra xem bien session array co ton tai khong, neu co ton tai thi list thong tin -->
			<?php 
				if(isset($_SESSION["product"]) == true)
				{
					foreach($_SESSION["product"] as $key=>$rows)
					{
			 ?>
			<tr>
				<td style="text-align: center;">
				<!-- ham file_exists(duongdan)//igzist- ton tai/ tra ve true neu file ton tai, nguoc lai se tra ve false -->
				<?php if(file_exists("upload/".$rows["anh"]) == true){ ?>
				<img src="upload/<?php echo $rows["anh"]; ?>" style="max-width: 100px;">
				<?php } ?>
				</td>
				<td><?php echo $rows["ten"]; ?></td>
				<td><?php echo $rows["gia"]; ?></td>
				<td style="text-align: center;">
					<a href="them-sua-san-pham.php?action=delete&key=<?php echo $key; ?>">Delete</a>
				</td>
			</tr>
				<?php } ?>
			<?php } ?>

			<?php echo session_save_path(); ?>
		</table>
	</fieldset>
</div>
</body>
</html>