<?php 
	//start session
	session_start();
	//kiem tra bien session product, neu khong ton tai thi khoi tao no
	if(isset($_SESSION["product"]) == false)
		$_SESSION["product"] = array();
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Them sua san pham</title>
	<meta charset="utf-8">
</head>
<body>
<style type="text/css">
	body{font-family: Arial}
	a{text-decoration: none;}
</style>
<div style="width: 1000px; margin:20px auto;">
	<?php include "menu.php"; ?>
	<!-- -------- -->
	<?php 
		//lay bien action tu url
		$action = isset($_GET["action"]) ? $_GET["action"]:"";
		//khai bao bien $form_action de luu action cua form
		$form_action = "";
		switch($action){
			case "add":
				$form_action = "them-sua-san-pham.php?action=do_add";
			break;
			case "do_add":
				$ten = $_POST["ten"];
				$gia = $_POST["gia"];
				//-----------
				$anh = "";
				//lay ten anh
				$anh = time().$_FILES["anh"]["name"];
				//thuc hien upload anh
			move_uploaded_file($_FILES["anh"]["tmp_name"], "upload/$anh");
				//-----------
				//gan gia tri vao bien session array
				$_SESSION["product"][] = array("ten"=>$ten,"gia"=>$gia,"anh"=>$anh);
				//di chuyen den trang index.php bang cu phap sau
				header("location:index.php");
			break;
			case "delete":
				$key = isset($_GET["key"])?$_GET["key"]:"";
				//duyet cac phan tu cua session array, neu key = bien key truyen vao tu url thi xoa phan tu do
				foreach($_SESSION["product"] as $k=>$v){
					if($k == $key){
						//xoa file bang ham unlink(duongdan)
						unlink("upload/".$_SESSION["product"][$key]["anh"]);
						//xoa phan tu cua array bang ham unset(tenarray[key])
						unset($_SESSION["product"][$key]);
					}
				}
				//di chuyen den trang index.php bang cu phap sau
				header("location:index.php");
			break;
		}
	 ?>
	<fieldset style="width: 300px; margin:auto;">
		<legend>Thêm sửa sản phẩm</legend>
		<form method="post" enctype="multipart/form-data" action="<?php echo $form_action; ?>">
			<table cellpadding="5">
				<tr>
					<td>Tên sp</td>
					<td><input type="text" required name="ten"></td>
				</tr>
				<tr>
					<td>Giá</td>
					<td><input type="number" min="0" value="0" name="gia"></td>
				</tr>
				<tr>
					<td>File</td>
					<td><input type="file" name="anh"></td>
				</tr>
				<tr>
				<tr>
					<td></td>
					<td><input type="submit" value="Process"></td>
				</tr>				
			</table>
		</form>
	</fieldset>
	<!-- -------- -->
</div>
</body>
</html>