<!DOCTYPE html>
<html>
<head>
	<title>Secsion & cookie</title>
	<meta charset="utf-8">
</head>
<body>
	<!-- 
		-Cookie : là đoạn mã lưu ở máy người dùng( phía client)
		- Cookie sẽ lưu theo thông tin sau:
			- Có thời gian tồn tại.
				VD: nếu thời gian tồn tại là 5p, thì sau 5p cookie sẽ tự động hủy
			- Hàm time() sẽ đổi thời gian ra thành số nguyên

			- Để tạo cookie, sử dụng cấu trúc: setcookie(tenbien,giatri,thoigiantontai)
			- Để lấy giá trị của biến cookie, sử dụng cấu trúc: $_COOKIE["tenbien"];

			-Để hủy cookie , chỉ cần đưa thời gian timeout về trước thoigiantontai




		- Session : lưu ở phía server
			- Để thực hiện biến session tồn tại trên trình duyệt, có nghĩa là ở tất cả các tab trong trình duyệt đó
			- Ứng với mỗi trình duyệt, sẽ có một session_id khác nhau. Khi tắt trình duyệt thì session cũng mất đi, có nghĩa là session tồn tại theo trình duyệt
			- Phải start session trước khi sử dụng bằng hàm session_start()
			- session id: session_id()
			-Khai báo biến session : $_SESSION["tenbien"]





		- Upload ảnh: 
			- Trong thẻ form , để upload ảnh bắt buộc phải có thuộc tính enctype = "multipart/form-data" ( vì dữ liệu ảnh là dữ liệu mã hóa k phải dl thường, mà muốn chuyền dl mã hóa phải có enctype)
			- Để lấy giá trị của file , dùng đối tượng $_FILES["tenformcontrol"]
				-$_FILES["tenformcontrol"][name] -> tên file
				-$_FILES["tenformcontrol"][size] -> kích thước file, tính bằng kb
				-$_FILES["tenformcontrol"][tmp_name] -> đường dẫn file upload vào thư mục tạm

			Di chuyển file từ thư mục tạm vào thư mục chỉ định bằng hàm move_uploaded_file(tmp_name,tenthumuc)
	 -->
</body>

	<?php 

		session_start();
		//ứng với mỗi trình duyệt sẽ có một session id khác nhau.
		echo "<h1>".session_id()."</h1>";
		// khoi tao bien session tenlop
		$_SESSION["tenlop"]= "PHP26";
		echo "<h1>Tên lớp:".$_SESSION["tenlop"]."</h1>";
	?>

	<!-- load menu.php -->

<style type="text/css">
	body{
		font-family: arial
	}

	a{
		text-decoration: none;
	}
</style>
	<div style="width: 1000px; margin: 10px auto;">
		<?php  include "menu.php" ?>
		<fieldset style="width: 600px; margin: 30px auto">
			<legend>Danh sách sản phẩm</legend>
			<table cellpadding="5" border="1" style="width: 100%; border-collapse: collapse;">
				<tr>
					<th style="width: 100px">Ảnh</th>
					<th>Tên sản phẩm</th>
					<th>Giá</th>
					<th style="width: 100px;"></th>
				</tr>

				<!-- kiểm tra xem biến session array có tồn tại hay không, nếu có tồn tại thì list thông tin lại -->
				<?php 
					if (isset($_SESSION["product"])== true) {
						# code...
						foreach ($_SESSION["product"] as $key => $rows) {
							# code...
						
				 ?>
				<tr>
					<td style="text-align: center;">
						<!-- hàm file exist(duongdan) trả về giá trị true tồn tại, ngược lại sẽ trả về false -->
						<?php 
							if(file_exists("upload/".$rows["anh"])==true){
								?>
								<img src="upload/<?php echo$rows["anh"]; ?>" style="max-width: 100px;">
							<?php } ?>
						 
					</td>
					<td><?php echo $rows["ten"]; ?></td>
					<td><?php echo $rows["gia"]; ?></td>
					<td style="text-align: center;">
						<a href="them-sua-san-pham.php?action=delete&key=<?php echo $key ?>"> Delete</a>
					</td>
				</tr>

					<?php } ?>
				<?php } ?>
			</table>
		</fieldset>
	</div>
</html>