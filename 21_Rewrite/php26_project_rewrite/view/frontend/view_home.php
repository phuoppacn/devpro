<?php 
    //lay các bản ghi thuộc danh mục sản phẩm
    $category = $this->model->get_all_record("select * from tbl_category_news order by pk_category_news_id desc limit 3,1");
    foreach($category as $rows_category)
    {
 ?>
 <?php 
    //kiem tra, neu danh muc co cac bai tin thi moi hien thi, neu khong thi khong hien thi
    //ham get_num_rows tra ve so luong ban ghi
    $check = $this->model->get_num_rows("select pk_news_id, c_name, c_img from tbl_news where fk_category_news_id=".$rows_category->pk_category_news_id." order by pk_news_id desc limit 0,1");
    if($check > 0)
    {
  ?>
<!-- list category -->
      <h5 class="box-main-title"><?php echo $rows_category->c_name; ?></h5>
      <div class="row"> 
      <?php 
          //lay mot tin moi nhat thuoc danh muc nay
          $first_news = $this->model->get_a_record("select * from tbl_news where fk_category_news_id=".$rows_category->pk_category_news_id." order by pk_news_id desc limit 0,1");
       ?>
        <!-- news -->
        <div class="col-md-6 col-sm-12">
          <article class="news">
            <figure> <img class="img-thumbnail" src="public/upload/news/<?php echo $first_news->c_img; ?>">
              <figcaption><a href="tin-tuc/chi-tiet/<?php echo remove_unicode($first_news->c_name)  ?>/<?php echo $first_news->pk_news_id ?>">

                <h6><?php echo $first_news->c_name; ?></h6>
                </a> </figcaption>
            </figure>
            <p><?php echo $first_news->c_description; ?></p>
          </article>
        </div>
        <!-- end news --> 

        <!-- news -->
        <div class="col-md-6 col-sm-12"> 
        <?php 
            //lay 4 tin tuc sau tin tuc dau tien
            $news = $this->model->get_all_record("select * from tbl_news where fk_category_news_id=".$rows_category->pk_category_news_id." order by pk_news_id desc limit 1,4");
            foreach($news as $rows)
            {
         ?>
          <!-- other news -->
          <article class="news">
            <div class="row">
              <div class="col-md-4"><img class="img-thumbnail" src="public/upload/news/<?php echo $rows->c_img; ?>"></div>
              <div class="col-md-8 no-padding"><a href="tin-tuc/chi-tiet/<?php echo remove_unicode($rows->c_name)  ?>/<?php echo $rows->pk_news_id ?>"><?php echo $rows->c_name; ?></a></div>
            </div>
            <div class="dotted"></div>
          </article>
          <!-- end other news --> 
          <?php } ?>


        </div>
        <!-- end news --> 
      </div>
      <!-- end list category --> 
      <?php } ?>
    <?php } ?>