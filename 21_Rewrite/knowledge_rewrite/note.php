file .htaccess sẽ quản lí cấu hình từ folder hiện tại đến các folder con

-	ghi chú sử dụng kí tự # ở đầu
-	chú ý: nếu file .htaccess thực hiện không đúng cú pháp từ toàn bộ ảnh , file trong thư mục đó và các thư mục con không truy cập được
- để thực hiện cấu hình cần bật chế độ rewrite theo cú pháp: RewriteEngine On

-rewrite URL theo cú pháp: 
	+/ RewriteRule ^duong-dan-ao$ duong-dan-thuc
	duong-dan-ao: la duong dan trực tiếp trên url : vd: hello/3/4
	duong-dan-thuc: đường dẫn chính xác để load website

	vd: RewriteRule ^hello/php26$ hello.php

- Load ảnh ở chế độ rewrite: cần đặt thẻ html5 bên trong thẻ head có cấu trúc 

<base href="duong dan goc/"> -> chú ý phải có dấu / ở cuối
