#include <iostream>
using namespace std;

class ARRAY {
    int n;
    float *a;
public:
    friend istream & operator>>(istream &x, ARRAY &y);
    friend ostream & operator<<(ostream &x, ARRAY y);
    float operator++();
    ARRAY operator/(ARRAY A);
    ARRAY();
    ARRAY(int N);
    ~ARRAY();
};

ARRAY::ARRAY() {
    n = 0;
    a = NULL;
}

ARRAY::ARRAY(int N) {
    n = N;
    a = new float[n];
    for(int i = 0; i < n; i++)
        a[i] = 0;
}

ARRAY::~ARRAY() {
    n = 0;
    delete [] a;
}

istream & operator>>(istream &x, ARRAY &y) {
    cout<<"\nNhap n: ";
    x>>y.n;
    y.a = new float[y.n];
    for (int i = 0; i < y.n; i++) {
        cout<<"\na["<<i<<"] = ";
        x>>y.a[i];
    }
    return x;
}

ostream & operator<<(ostream &x, ARRAY y) {
    for (int i = 0; i < y.n; i++)
        x<<"\na["<<i<<"] = "<<y.a[i];
    return x;
}

float ARRAY::operator++() {
    float tong = 0;
    for (int i = 0; i < n; i++)
        tong += a[i];

    return (float) tong/n;
}

ARRAY ARRAY::operator/(ARRAY A) {
    ARRAY R(A.n);
    //R.n = A.n;
    //R.a = new float[R.n];

    if (n == A.n) {
        for (int i = 0; i < R.n; i++)
            R.a[i] = a[i]/A.a[i];
        return R;
    }
}

int main()
{
    ARRAY A,B;

    cout<<"\nNhap mang A: ";
    cin>>A;
    cout<<"\nMang A: "<<A;

    cout<<"\nNhap mang B: ";
    cin>>B;
    cout<<"\nMang B: "<<B;

    cout<<"\nTb cua mang A: "<<++A;
    cout<<"\nTb cua mang B: "<<++B;

    //ARRAY C = A/B;
    cout<<"\nA/B = "<<A/B;

    return 0;
}
