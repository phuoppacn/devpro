<!DOCTYPE html>
<html lang="vi">
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<title>Thế giới di động</title>
<link rel="shortcut icon" href="public/frontend/images/favicon.png"/>
<!-- Bootstrap -->
<link href="public/frontend/css/bootstrap.min.css" rel="stylesheet"/>
<!-- Custom styles for this template -->
<link href="public/frontend/css/style.css" rel="stylesheet"/>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="public/frontend/js/html5shiv.js"></script>
    <script src="public/frontend/js/respond.min.js"></script>
    <![endif]-->

<script type="text/javascript" src="public/frontendjs/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
 function load_more(p){
      //--------
      
      $(document).ready(function(){
    //bat su kien the a co id " btn- load -more" click
    //chon doi tuong la ajax
    $.ajax({
      url:"page_ajax_product.php?p=<?php echo $_GET["p"]; ?>"
      type: "GET";
      dataType: "html";

      //chu y success k co ;
      success:function(result){
        alert(result);
      }

    });
      //
    }); 
//////
 } 
</script>
</head>

<body>
<nav class="navbar navbar-inverse" role="navigation">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand" href="."><i class="glyphicon glyphicon-phone"></i> Thế giới di động</a> </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li> <a href="index.php?controller=cart"><i class="glyphicon glyphicon-shopping-cart"></i> Giỏ hàng : 1 sản phẩm</a> </li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>
<div class="container">
  <div class="row">
    <div class="col-lg-3 col-sm-4 col-xs-12 pull-right" id="sidebar" role="navigation">
 <?php 
//thực hiện load file sau khi cắt 
 include "controller/frontend/controller_category.php";
  ?>
    </div>
    <div class="col-lg-9 col-sm-8 col-xs-12 pull-left"> 
      <!-- BEGIN CONTENT -->
            <?php 
        //lay bien controller tu url
        $controller = isset($_GET["controller"]) ? $_GET["controller"]:"";
        //kiểm tra, nếu $controller có giá trị và tồn tại controller đó
        if($controller != "" && file_exists("controller/frontend/controller_$controller.php")){
          //ghi cac thanh phan de thanh duong dan vat ly
          $controller = "controller/frontend/controller_$controller.php";        
        }else
          $controller = "controller/frontend/controller_ajax.php";
          //$controller = "controller/frontend/controller_home1.php";
          //$controller = "controller/frontend/controller_home.php";
        if(file_exists($controller))
          include $controller;
     ?> 
      <!-- END CONTENT --> 
    </div>
    <!--/span--> 
  </div>
  <!--/row--> 
</div>
<!--/.container--> 

<script type="text/javascript" src="public/frontend/js/jquery-1.10.2.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<script type="text/javascript" src="public/frontend/js/bootstrap.min.js"></script> 
<script type="text/javascript">
        $(document).ready(function () {
            $('#sidebar .panel-heading').click(function () {
                $('#sidebar .list-group').toggleClass('hidden-xs');
                $('#sidebar .panel-heading b').toggleClass('glyphicon-plus-sign').toggleClass('glyphicon-minus-sign');
            });
        });
    </script>
</body>
</html>