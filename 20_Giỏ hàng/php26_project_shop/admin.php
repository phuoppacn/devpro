<?php 
	session_start();
	//load file config
	include "config.php";
	//load file model
	include "model/model.php";
	//Kiểm tra, nếu user chưa đăng nhập thì hiển thị MVC login, nếu user đã đăng nhập thì hiển thị file master.php
	if(isset($_SESSION["c_email"]) == false)
		include "controller/backend/controller_login.php";
	else{
		//user da dang nhap		
		include "view/backend/master.php";
	}
 ?>