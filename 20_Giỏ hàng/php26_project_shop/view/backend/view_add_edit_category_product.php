<div class="row justify-content-center">
	<div class="col-md-10 col-md-offset-1">
		<div class="card border-primary">
			<div class="card-header text-white bg-primary">Add edit category_product</div>
			<div class="card-body">
				<form method="post" action="<?php echo $form_action; ?>">				
					<!-- row -->
					<div class="form-group">
						<div class="row">
							<div class="col-md-2">Fullname</div>
							<div class="col-md-10">
	<input type="text" name="c_name" value="<?php echo isset($arr->c_name)?$arr->c_name:""; ?>" class="form-control">
							</div>
						</div>
					</div>
					<!-- end row -->
					<!-- row -->
					<div class="form-group">
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-10">
	<input type="submit" class="btn btn-primary" value="Process"> 
	<input type="reset" class="btn btn-danger" value="Reset">
	 						</div>
						</div>
					</div>
					<!-- end row -->
				</form>
			</div>
		</div>
	</div>
</div>