<div class="row justify-content-center">
	<div class="col-md-10 col-md-offset-1">
		<div class="card border-primary">
			<div class="card-header bg-primary text-white">Add edit user</div>
			<div class="card-body">
				<form method="post" action="<?php echo $form_action; ?>">
					<!-- row -->
					<div class="form-group">
						<div class="row">
							<div class="col-md-2">Email</div>
							<div class="col-md-10">
	<input type="email" name="c_email" value="<?php echo isset($arr->c_email)?$arr->c_email:""; ?>" <?php echo isset($arr->c_email)?"disabled":"required"; ?> class="form-control">
							</div>
						</div>
					</div>
					<!-- end row -->
					<!-- row -->
					<div class="form-group">
						<div class="row">
							<div class="col-md-2">Password</div>
							<div class="col-md-10">
	<input type="password" name="c_password" <?php echo isset($arr->c_email)?"placeholder='Nhập password mới nếu muốn đổi password'":"required"; ?> class="form-control">
							</div>
						</div>
					</div>
					<!-- end row -->
					<!-- row -->
					<div class="form-group">
						<div class="row">
							<div class="col-md-2">Fullname</div>
							<div class="col-md-10">
	<input type="text" name="c_fullname" value="<?php echo isset($arr->c_fullname)?$arr->c_fullname:""; ?>" class="form-control">
							</div>
						</div>
					</div>
					<!-- end row -->
					<!-- row -->
					<div class="form-group">
						<div class="row">
							<div class="col-md-2"></div>
							<div class="col-md-10">
	<input type="submit" class="btn btn-primary" value="Process"> 
	<input type="reset" class="btn btn-danger" value="Reset">
	 						</div>
						</div>
					</div>
					<!-- end row -->
				</form>
			</div>
		</div>
	</div>
</div>