<div class="row">
  <div class="col-md-12"><h4>
    <?php 
      $category = $this->model->get_a_record("select c_name from tbl_category_product where pk_category_product_id=$category_id");
      echo isset($category->c_name)?$category->c_name:"";
    ?>
  </h4></div>
</div>
<div class="row product-list">
      <?php 
          foreach($arr as $rows)
          {
       ?>
        <div class="col-lg-4 col-sm-6">
          <div class="thumbnail"> <a href="index.php?controller=product_detail&id=<?php echo $rows->pk_product_id; ?>"> <img src="public/upload/product/<?php echo $rows->c_img; ?>" alt="<?php echo $rows->c_name; ?>"/> </a>
            <div class="caption text-center">
              <h4><a href="index.php?controller=product_detail&id=<?php echo $rows->pk_product_id; ?>"><?php echo $rows->c_name; ?></a></h4>
              <p><?php echo number_format($rows->c_price); ?> VNĐ</p>
              <p><a href="index.php?controller=cart&action=add&id=<?php echo $rows->pk_product_id; ?>" class="btn btn-primary" role="button">Đặt mua</a></p>
            </div>
          </div>
        </div>
        <?php } ?>
        
      <div style="clear: both;"></div>            
      <div class="text-center">
        <ul class="pagination">
          <li class="disabled"><span>Trang</span></li>
          <?php 
              for($i = 1; $i <= $num_page; $i++)
              {
           ?>
          <li><span><a href="index.php?controller=product&category_id=<?php echo $category_id ?>&p=<?php echo $i; ?>"><?php echo $i; ?></a></span></li>
          <?php } ?>
        </ul>
      </div>