<style type="text/css">
	.table th, .table td {
		text-align: center;
	}
	.table th:nth-child(3), .table td:nth-child(3)  {
		width: auto;
		text-align: left;
	}
	.table th:nth-child(4), .table td:nth-child(4),
	.table th:nth-child(5), .table td:nth-child(5)  {
	}
	.table td {
		vertical-align: middle!important;
	}
</style>
    <form id="cart_form" method="post" action="index.php?controller=cart&action=update" role="form">
      <div class="col-xs-12">
        <h2>Giỏ hàng</h2>
        <table class="table table-bordered table-hover">
          <thead>
            <tr>
              <th class="hidden-xs">STT</th>
              <th class="hidden-xs">Ảnh</th>
              <th>Sản phẩm</th>
              <th>Giá</th>
              <th>Số lượng</th>
              <th>Tác vụ</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              foreach ($_SESSION["cart"] as $product) {
            ?>
            <tr>
              <td class="hidden-xs">2</td>
              <td class="hidden-xs"><image src="public/frontend/upload/product/<?php echo $product["c_img"] ?>" style="max-width:50px; max-height:50px;" /></td>
              <td><a href="index.php?controller=product&amp;action=detail&amp;pid=7"><?php echo $product["c_name"] ?></a></td>
              <td> <?php echo number_format($product["c_price"]); ?></td>
              <td><div class="btn-group">
                  <input name="product_<?php echo $product["pk_product_id"] ?>" style="width: 80px;" min="0" type="number" value="<?php echo $product["number"]; ?>" class="form-control text-center"/>
                </div></td>
              <td><a href="index.php?controller=cart&action=delete&id=<?php echo $product["pk_product_id"] ?>" class="text-danger"><i class="glyphicon glyphicon-remove"></i></a></td>
            </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="6">Thành tiền : <?php echo number_format($this->cart_total());?> VNĐ
                </th>
            </tr>
          </tfoot>
        </table>
        <div class="form-group"> 
          <!-- Single button -->
          <div class="btn-group">
            <input type="submit" class="form-control" value="Cập nhật"/>
          </div>
          <a href="index.php?controller=order&action=order" class="btn btn-primary"><i class="glyphicon glyphicon-list-alt"></i> Đơn hàng</a> </div>
      </div>
    </form>