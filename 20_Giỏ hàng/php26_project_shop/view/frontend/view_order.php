<div class="row">
	<div class="col-md-6 col-md-offset-3">
		<form method="post" action="index.php?controller=order&action=checkout"> 
			<h2>Thông tin khách hàng</h2>
	        <div class="form-group">
	          <input name="name" type="text" class="form-control" placeholder="Họ và tên" required="required"/>
	        </div>
	        <div class="form-group">
	          <input name="address" type="text" class="form-control" placeholder="Địa chỉ" required="required"/>
	        </div>
	        <div class="form-group">
	          <input name="phone" type="text" class="form-control" placeholder="Số di động" required="required" pattern="[0-9]{10,11}"/>
	        </div>
	        <div class="form-group">
	          <textarea class="form-control" name="note"  placeholder="Ghi chú"></textarea>
	        </div>
	        <div class="form-group">
	          <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i> Đặt hàng</button>
	        </div>
        </form>
	</div>
</div>