<?php 
	class controller_category_product{
		public $model;
		public function __construct(){			
			$this->model = new model();
			//-------
			$act = isset($_GET["act"])?$_GET["act"]:"";
			switch($act){
				case "delete":
					$id = isset($_GET["id"])&&is_numeric($_GET["id"])?$_GET["id"]:0;
					//xoa ban ghi
					$this->model->execute("delete from tbl_category_product where pk_category_product_id=$id");
					header("location:admin.php?controller=category_product");
				break;
			}
			//-------
			//khai bao so ban ghi tren mot trang
			$record_per_page = 15;
			//tong so ban ghi
			$total_record = $this->model->get_num_rows("select pk_category_product_id from tbl_category_product");
			//số trang = tongsobanghi/sobanghitrentrang;
			$num_page = ceil($total_record/$record_per_page);
			//lay bien p truyen tu url(bien nay chi trang hien tai)
			$p = isset($_GET["p"])&&is_numeric($_GET["p"])&&$_GET["p"]>=1 ? ($_GET["p"]-1):0;
			//tu trang hien tai, xac dinh lay tu ban ghi nao den ban ghi nao
			$from = $p * $record_per_page;
			//goi ham fetch() cua class model de truyen vao cau lenh sql
			$arr = $this->model->get_all_record("select * from tbl_category_product order by pk_category_product_id desc limit $from,$record_per_page");
			//load view
			include "view/backend/view_category_product.php";
			//-------
		}
	}
	new controller_category_product();
 ?>