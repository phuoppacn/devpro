<!DOCTYPE html>
<html>
<head>
	<title>Array</title>
	<meta charset="utf-8">

</head>
<body>
	<!-- 
		-Array : mảng trong php
			-key: là chỉ số của array, chỉ số từ 0-n;
			-value: là giá trị tương ứng với phần tử thứ key
			-xuất giá trị tại phần tử thứ key : $tenarrayy[key]
			-thêm phần tử vào array theo cú pháp: $tenarray[]=giatri, khi do key sẽ tự động tăng thêm 1
		-Khai báo array: 
			- sử dụng từ khóa array và truyền các giá trị tương ứng
				VD: $bien= array(1,"hello", true //con co the rat nhieu gia tri trong nay)
		-Hàm print_r(tenbien) sẽ xuât ra cấu trúc của array
		-Hàm count($tenarray) sẽ trả về số lượng phần tử của array

		-Hàm foreach($tenarray as $key=>$value){} sẽ duyệt qua các phần tử của array tương ứng với $key, $value
			VD: $arr[0]=1;
				// $arr[1]=4;
				$arr[2]=5;
				$arr[3]=6;

				khi comment phần tử 1 : sẽ báo lỗi trên trình duyệt


	

		
	 -->

	<?php
		// $arr = array(1,4,5,6);

		$arr[0]=1;
		// $arr[1]=4;
		$arr[2]=5;
		$arr[3]=6;
		// tìm số lớn nhất
		// duyệt các phần tử mảng đề tìm số lớn nhất

		echo "<prev>";
		print_r($arr);
		echo "</prev>";
		$max=$arr[0];
		for($i=0; $i<count($arr); $i++)
		{
			if($arr[$i]>$max)
				$max=$arr[$i];
		}

	

		echo "Số lớn nhất là :". $max;

		foreach ($arr as $key => $value) {
			# code...
			if($arr[$key] >$max)
				$max= $arr[$key];
		}


		// hàm foreach sẽ duyệt từng phần tử 1 cho đến hết số phần tử, không như hàm for nếu gặp lỗi sẽ dừng lại ở phần tử đó
		echo "<br>Số lớn nhất là :". $max;



		// hàm foreach($tenarray as $value){} sẽ duyệt qua các phần tử của array để lấy value
		foreach ($arr as $key => $value) {
			# code...
			echo "<h1>Key: $key -value: $value </h1>";
		}

		foreach ($arr as $v) {
			# code...
			echo "<h1> value: $v</h1>";
		}

	?>

	<?php

		$arr = array('hoten' =>"Nguyen van phu" ,'lop'=>'php26' );
		$arr["nam"]= 2018;
		echo "<pre>";
			print_r($arr);
		echo "</pre>";
	?>



<!-- mảng trong mảng -->

	<?php 
	echo "<h1>Mảng trong mảng </h1>";
		$arr = array(
			// -Có thể định nghĩa tên key theo kiểu cấu trúc: array("tenkey1"=>value1, "tenkey2"=>value2....)
			array('hoten' => "Nguyen van phu", "email" =>"bizinphu@gmai" ),
			array('hoten' => "Nguyen van b", "email" =>"bizinphu@gmai" ),
			array('hoten' => "Nguyen van c", "email" =>"bizinphu@gmai" ),
			array('hoten' => "Nguyen van d", "email" =>"bizinphu@gmai" ),
			array('hoten' => "Nguyen van 3", "email" =>"bizinphu@gmai" ),
			array('hoten' => "Nguyen van e", "email" =>"bizinphu@gmai" ),
			array('hoten' => "Nguyen van f", "email" =>"bizinphu@gmai" ) );
	 ?>

	 <fieldset style="width: 400px; margin: 20px auto">
	 	<legend>Danh sách sinh viên</legend>
	 	<table cellpadding="5" border="1" style="width: 100%; border-collapse: collapse;">
	 		<tr>
	 			<th>Họ và tên</th>
	 			<th>Email</th>
	 		</tr>
	 		<?php
	 			foreach ($arr as $rows) 
	 			{
	 				# code...
	 		?>
	 				<tr>
	 					<td><?php echo $rows["hoten"]; ?> </td>
	 					<td><?php echo $rows["email"]; ?> </td>
	 				</tr>
	 			
	 		<?php } ?>

	 		<!-- php sẽ được thực hiện trước -->
	 	</table>
	 </fieldset>
</body>
</html>