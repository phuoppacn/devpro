<!DOCTYPE html>
<html>
<head>
	<title>Array</title>
	<meta charset="utf-8">

</head>
<body>
	<!-- 
		-Array : mảng trong php
			-key: là chỉ số của array, chỉ số từ 0-n;
			-value: là giá trị tương ứng với phần tử thứ key
			-xuất giá trị tại phần tử thứ key : $tenarrayy[key]
			-thêm phần tử vào array theo cú pháp: $tenarray[]=giatri, khi do key sẽ tự động tăng thêm 1
		-Khai báo array: 
			- sử dụng từ khóa array và truyền các giá trị tương ứng
				VD: $bien= array(1,"hello", true //con co the rat nhieu gia tri trong nay)
		-Hàm print_r(tenbien) sẽ xuât ra cấu trúc của array
		-Hàm count($tenarray) sẽ trả về số lượng phần tử của array

	 -->

	<?php
		// key chạy từ 0-n
		$arr =array ("Nokia","samsung","iphone","LG","Lenovo",);
		echo "<h1> $arr[4]</h1>";

		echo "<pre>";
			print_r($arr);
		echo "</pre>";
		echo count($arr);
		//them phần tử vào array

		$arr[]="mobile";

		echo "<pre>";
			print_r($arr);
		echo "</pre>";
		echo "Số phần tử của mảng là : ".count($arr);


	?>
</body>
</html>