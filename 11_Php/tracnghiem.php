<!DOCTYPE html>
<html>
<head>
	<title>Trắc nghiệm</title>
	<meta charset="utf-8">
</head>
<body>
<!--
	include "tenfile" sẽ load file tenfile vào file hiện tại
	include_once "tenfile" sẽ load file tenfile vào file hiện tại
	require "tenfile" sẽ load file tenfile vào file hiện tại
	require_once "tenfile" sẽ load file tenfile vào file hiện tại

 -->
<?php
	//load file data.php vao day
	include "data.php";
	include "data.php";
	// include: 1 hàm chỉ được định nghĩa 1 lần nếu include 2 lần giống nhau thì hàm bên data không thể định nghĩa lại lần 2 nên sẽ báo lỗi, còn với include_once : sẽ tự dịch vào 1 cái và k báo lỗi
	// require: 
?>
</body>
</html>