<!DOCTYPE html>
<html>
<head>
	<title>Tạo form submit dữ liệu</title>
	<meta charset="utf-8">
</head>
<body>
<!-- tạo nut submit de toi trang thu 2

	-Cac thuoc tinh trong form:
		-mothod: mac dinh cua method se la get, neu muon truyen theo phuong thuc post thi phai dat post vao
		- action :  trang ma form se submit toi. neu khong ghi ten trang thi khi submit se submit lại chính nó

		-Thuộc tính required trong form control: bắt buộc phải nhập thông tin thường dùng cho textbox, textarea, file , number...
 -->

 <script type="text/javascript">
 	function submitform(){
 		// body...
 		//alert("ok");
 		document.getElementById("frm").submit();
 	}
 </script>
<fieldset style="width: 400px; margin: 15px auto">
	<legend>Form submit</legend>
	<form method="post" id="frm" action="page2.php?a=5&b=hello" onsubmit="return true;">
		<!-- 
			- Sau dấu ? là danh sách các biến truyền theo kiểu Get
			-Cấu trúc: tenbien=giatri
			-Các biến cách nhau bằng dấu &
			
			mặc định của onsubmit là true: thì form mới thực hiện submit, nếu để là false sẽ k thực hiện được -->
		<input type="number" style="width: 50px;" name="so1" required>
		<select name="pheptoan">
			<option value="+">+</option>
			<option value="-">-</option>
			<option value="*">*</option>
			<option value="/">/</option>

		</select>
		<input type="number" name="so2" style="width: 50px;" required>
		<input type="submit" name="" value="Thực hiện">
		<!-- khi type là submit thì form sẽ thực hiện như bình thường và chuyển đến trang khác, còn với button và ảnh thì phải thực hiện gọi javascrips -->
		<input type="button" value="Button" onclick="return submitform();">

			<a href="#" onclick="return submitform();">Click here</a>
	</form>
</fieldset>
</body>
</html>