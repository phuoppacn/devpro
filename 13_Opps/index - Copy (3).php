<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">
<!-- 
	- Đối tượng là những sự vật sự việc có thể định hình được( là danh từ)
	- Thuộc tính: mô tả tính chất của sự vật, sự việc
	- Phương thức là cách thực hiện sự vật , sự việc
	- Trong php: đối tượng được khai báo bằng từ khóa class.
		VD: class A{}

 -->
</head>
<body>

	<?php
		// Khai báo biến array trong class 
		class sinhvien{

			public $thongtin = array();
			public function them($ht,$email)
			{
				// tác động vào biến array
				$this->thongtin[]= array("hoten"=>$ht, "email"=> $email);
			}

			public function hienthi(){

				// include file sv.php 
				include 'sv.php';
				// thực thi file sv.php chứ không phải load nguyên mẫu rồi thực thi
			}
		}
		// end class
		// khai báo đổi tượng $sv;

		$sv = new sinhvien();
		$sv->them("Nguyen van phu", "phu@gmail.com");
		$sv->them("Nguyen van b", "b@gmail.com");
		$sv->them("Nguyen van c", "c@gmail.com");
		$sv->them("Nguyen van d", "d@gmail.com");
		$sv->them("Nguyen van e", "e@gmail.com");
		$sv->them("Nguyen van g", "g@gmail.com");

		$sv->hienthi();

	?>
</body>
</html>