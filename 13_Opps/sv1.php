<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">
<!-- 
	- Đối tượng là những sự vật sự việc có thể định hình được( là danh từ)
	- Thuộc tính: mô tả tính chất của sự vật, sự việc
	- Phương thức là cách thực hiện sự vật , sự việc
	- Trong php: đối tượng được khai báo bằng từ khóa class.
		VD: class A{}

 -->
</head>
<body>

<fieldset style="width: 400px; margin: auto">
	<legend>Thông tin sinh viên</legend>
	<form action="" method="post">
		<table cellpadding="5">
			<tr>
				<td>Họ tên</td>
				<td><input type="text" name="hoten" required>
				</td>
			</tr>
			<tr>
				<td>Email</td>
				<td><input type="email" name="email" required>
				</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" value="Thêm mới">
				</td>
			</tr>
		</table>
	</form>
</fieldset>
<fieldset style="width: 400px; margin: 20px auto;">
	<legend>Danh sách sinh viên</legend>
	<table cellpadding="5" border="1" style="width: 100%; border-collapse: collapse;">
		<tr>
			<th>Họ và tên</th>
			<th>Email</th>
			<th style="width: 100px;"></th>
		</tr>
		<?php 
			foreach ($_SESSION["sv"] as $rows) { ?> 
				

			<tr>
				<td><?php echo $rows["hoten"] ?></td>
				<td><?php echo $rows["email"] ?></td>
				<td style="text-align: center;">
					<a href="#" <?php unset($rows["hoten"]); unset($rows["email"]); ?>>Delete</a>
				</td>
			</tr>
			<?php } ?>
		 
	</table>
</fieldset>
	<?php
		
	?>
</body>
</html>