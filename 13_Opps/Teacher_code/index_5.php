<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">	
</head>
<!-- 
- Đối tượng: là những sự vật, sự việc có thể định hình được
- Thuộc tính: mô tả tính chất của sự vật, sự việc
- Phương thức: là cách thức thực hiện của sự vật, sự việc
- Trong PHP: đối tượng được khai báo bằng từ khoá class. VD: class A{}
 -->
<body>
<?php 
	//sử dụng biến array trong class
	class sinhvien{
		public $thongtin = array();
		public function them($ht,$email){
			//tác động vào biến array
			$this->thongtin[] = array("hoten"=>$ht,"email"=>$email);
		}
		public function hienthi(){
			//include file sv.php
			include "sv.php";
		}
	}
	//khai bao object $sv
	$sv = new sinhvien();
	$sv->them('Nguyễn Văn A','nva@mail.com');
	$sv->them('Nguyễn Văn B','nvb@mail.com');
	$sv->them('Nguyễn Văn C','nvc@mail.com');
	$sv->them('Nguyễn Văn D','nvd@mail.com');
	$sv->them('Nguyễn Văn E','nve@mail.com');
	$sv->hienthi();
 ?>
</body>
</html>