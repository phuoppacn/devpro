<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">	
</head>
<!-- 
- Đối tượng: là những sự vật, sự việc có thể định hình được
- Thuộc tính: mô tả tính chất của sự vật, sự việc
- Phương thức: là cách thức thực hiện của sự vật, sự việc
- Trong PHP: đối tượng được khai báo bằng từ khoá class. VD: class A{}
 -->
<body>
<?php 
	class xehoi{
		//khai báo thuộc tính trong class (thực chất là các biến bên trong class) bằng từ khoá public, protected, private
		public $mausac = "trang";
		public $thuonghieu = "BMV";
		//định nghĩa phương thức (thực chất là các hàm bên trong class) bằng cách dùng từ khoá public, protected, privite + function + tenham()
		public function thongtin(){
			//để tác động vào một biến (thuộc tính) của class, sử dụng cú pháp: $this->tenbien
			echo "<h1>Màu sắc: ".$this->mausac."</h1>";
			echo "<h1>Thương hiệu: ".$this->thuonghieu."</h1>";
		}
	}
	//để sử dụng class, cần khai bảo một object của class đó
	$xh = new xehoi();
	//từ đối tượng vừa khai báo, để tác động vào một hàm bên trong class, sử dụng cấu trúc: tendoituong -> tenham();
	$xh->thongtin();
 ?>
</body>
</html>