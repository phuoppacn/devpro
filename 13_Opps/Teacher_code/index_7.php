<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">	
</head>
<!-- 
- Đối tượng: là những sự vật, sự việc có thể định hình được
- Thuộc tính: mô tả tính chất của sự vật, sự việc
- Phương thức: là cách thức thực hiện của sự vật, sự việc
- Trong PHP: đối tượng được khai báo bằng từ khoá class. VD: class A{}
- Kế thừa: class con kế thừa class cha theo cú pháp: class con extedns cha. Khi đó class con sẽ sử dụng được các hàm, các biến của class cha
- Tầm vực của các từ khoá: public, protected, private
	- public: có thể tác động được
		- từ bên ngoài class
		- bên trong chính class đó
		- class nó được kế thừa
	- protected: có thể tác động được
		- bên trong chính class đó
		- class nó được kế thừa
		- bên ngoài không tác động được vào bên trong class
	- private
		- bên trong chính class đó
		- class được kế thừa không tác động được
		- bên ngoài không tác động được vào bên trong class
 -->
<body>
<?php 
	class cha{
		public function str_cha(){
			echo "<h1>Cha: Hello world</h1>";
		} 
		public function str(){
			echo "<h1>Cha: Test</h1>";
		}
	}
	class con extends cha{
		public function str_con(){
			echo "<h1>Con: Hello world</h1>";
			$this->str_cha();
		}
		public function str(){
			echo "<h1>Con: Test</h1>";
			//gọi lên hàm của class cha (khi các hàm trùng tên nhau), sử dụng từ khoá parent::tenham()
			parent::str();
		}
	}
	//khoi tao object cua class con
	$c = new con();
	$c->str_con();
	$c->str();
 ?>
</body>
</html>