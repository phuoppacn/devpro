<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">	
</head>
<!-- 
- Đối tượng: là những sự vật, sự việc có thể định hình được
- Thuộc tính: mô tả tính chất của sự vật, sự việc
- Phương thức: là cách thức thực hiện của sự vật, sự việc
- Trong PHP: đối tượng được khai báo bằng từ khoá class. VD: class A{}
- Kế thừa: class con kế thừa class cha theo cú pháp: class con extedns cha. Khi đó class con sẽ sử dụng được các hàm, các biến của class cha
- Tầm vực của các từ khoá: public, protected, private
	- public: có thể tác động được
		- từ bên ngoài class
		- bên trong chính class đó
		- class nó được kế thừa
	- protected: có thể tác động được
		- bên trong chính class đó
		- class nó được kế thừa
		- bên ngoài không tác động được vào bên trong class
	- private
		- bên trong chính class đó
		- class được kế thừa không tác động được
		- bên ngoài không tác động được vào bên trong class
 -->
<body>
<?php 
	//muốn sử dụng session, phải start_session()
	session_start();
	//kiểm tra, nếu session sv chưa tồn tại thì khởi tạo
	if(isset($_SESSION["sv"]) == false)
		$_SESSION["sv"] = array();
	//tạo class sinhvien
	class sinhvien{
		//khai báo hàm tạo
		public function __construct(){
			// delete phần tử trong session array 
			if(isset($_GET["action"]) && $_GET["action"] =='delete')
			{
				$key= $_GET['key'];
				foreach ($_SESSION["sv"] as $k => $v) {
					# code...\
					if($k == $key)
							unset($_SESSION['sv'][$key]);
				}

				// di chuyển tới trang header.php để ẩn key 
				header("location:index1.php");
			}
			//kiem tra, neu trang o trang thai POST thi thuc hien them thong tin vao session array
			if($_SERVER["REQUEST_METHOD"] == "POST"){
				$hoten = $_POST["hoten"];
				$email = $_POST["email"];
				//goi ham them de them phan tu vao array
				$this->them($hoten,$email);
			}
			//goi ham hien thi thong tin sinh vien
			$this->hienthi();
		}
		//ham them sinh vien
		public function them($hoten,$email){
			$_SESSION["sv"][] = array("hoten"=>$hoten,"email"=>$email);
			//di chuyen lai ve trang index.php
			header("location:index1.php");
		}
		//ham hien thi thong tin sinh vien
		public function hienthi(){
			//load file sv1.php
			include "sv1.php";
		}
		public function xoa($hoten,$email)
		{
			include "sv1.php";
		}
	}
	new sinhvien();
 ?>
</body>
</html>