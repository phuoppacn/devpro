<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">	
</head>
<!-- 
- Đối tượng: là những sự vật, sự việc có thể định hình được
- Thuộc tính: mô tả tính chất của sự vật, sự việc
- Phương thức: là cách thức thực hiện của sự vật, sự việc
- Trong PHP: đối tượng được khai báo bằng từ khoá class. VD: class A{}
- Kế thừa: class con kế thừa class cha theo cú pháp: class con extedns cha. Khi đó class con sẽ sử dụng được các hàm, các biến của class cha
- Tầm vực của các từ khoá: public, protected, private
	- public: có thể tác động được
		- từ bên ngoài class
		- bên trong chính class đó
		- class nó được kế thừa
	- protected: có thể tác động được
		- bên trong chính class đó
		- class nó được kế thừa
		- bên ngoài không tác động được vào bên trong class
	- private
		- bên trong chính class đó
		- class được kế thừa không tác động được
		- bên ngoài không tác động được vào bên trong class
 -->
<body>
<?php 
	class A{
		public function a1(){
			echo "<h1>A1: Hello world</h1>";
		}
	}
	class B{
		public function b1(){
			//khởi tạo một object của class A, để từ đó có thể tác động được vào các hàm, các biến trong class A
			$obj = new A();
			$obj->a1();
		}
	}
	$test = new B();
	$test->b1();
 ?>
</body>
</html>