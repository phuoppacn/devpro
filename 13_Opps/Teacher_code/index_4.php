<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">	
</head>
<!-- 
- Đối tượng: là những sự vật, sự việc có thể định hình được
- Thuộc tính: mô tả tính chất của sự vật, sự việc
- Phương thức: là cách thức thực hiện của sự vật, sự việc
- Trong PHP: đối tượng được khai báo bằng từ khoá class. VD: class A{}
 -->
<body>
<?php 
	//tạo class có tên là pheptoan, thực hiện các phép tính cộng, trừ, nhân, chia
	//khai báo class pheptinh
	class pheptinh{
		//khai báo 2 biến: so1,so2
		public $so1,$so2;
		//xây dựng hàm tạo để truyền 2 số vào, thực hiện gán 2 số đó cho 2 biến $so1, $so2
		public function __construct($s1,$s2){
			$this->so1 = $s1;
			$this->so2 = $s2;
		}
		public function cong(){
			$ketqua = $this->so1 + $this->so2;
			echo "<h1>$this->so1 + $this->so2 = $ketqua</h1>";
		}
		public function tru(){
			$ketqua = $this->so1 - $this->so2;
			echo "<h1>$this->so1 - $this->so2 = $ketqua</h1>";
		}
		public function nhan(){
			$ketqua = $this->so1 * $this->so2;
			echo "<h1>$this->so1 * $this->so2 = $ketqua</h1>";
		}
		public function chia(){
			$ketqua = $this->so1 / $this->so2;
			//hàm ceil(so) sẽ lấy giá trị trần
			//hàm floor(so) sẽ lấy giá trị sàn
			$ketqua = ceil($ketqua);			
			echo "<h1>$this->so1 / $this->so2 = $ketqua</h1>";
		}
	}
	//khai báo biến $pt là một object của class pheptinh
	$pt = new pheptinh(5,3);
	//gọi hàm cộng
	$pt->cong();
	$pt->tru();
	$pt->nhan();
	$pt->chia();
 ?>
</body>
</html>