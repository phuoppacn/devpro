<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">
<!-- 
	
 -->
</head>
<body>

	<?php
//không được khai báo tên class trùng với tên hàm
		class A{
			public function a1(){
				echo "<h1> A1: hello world</h1>";
			}
		}
		class B{
			public function b1(){
				//khởi tạo một biến object của class A, để từ đó có thể tác động được vào các hàm, các biến trong A

				// Đây là một cách khác mà không cần dùng kế thừa, có thể khai báo ở bất kì đâu
				$obj= new A();
				$obj -> a1();
				// echo "<h1> A1: hello world</h1>";
			}
		}

		$test = new B();
		$test ->b1();
	?>
</body>
</html>