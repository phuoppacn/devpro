<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">
<!-- 
	- Đối tượng là những sự vật sự việc có thể định hình được( là danh từ)
	- Thuộc tính: mô tả tính chất của sự vật, sự việc
	- Phương thức là cách thực hiện sự vật , sự việc
	- Trong php: đối tượng được khai báo bằng từ khóa class.
		VD: class A{}

 -->
</head>
<body>

	<?php
		$str= "hello world";
		class xehoi{
			// khai báo thuộc tính trong class( thực chát là các biến bên trong class) bằng từ khóa public, protected, private

			public $mausac ="trang";
			public $thuonghieu = "BMV";
			// hàm tạo : là hàm được gọi đầu tiên khi class khởi tạo , hàm tạo được định nghĩa bằng từ khóa __construct()

			public function __construct(){
				echo "<h1> Hàm tạo được gọi là</h1>";
			}
			// Hàm hủy là hàm được gọi cuối cùng trước khi class được khỏi tạo xong. Hàm hủy được định nghĩa bằng từ khóa : __destruct()


			public function __destruct(){
				echo "<h1> Hàm hủy được gọi</h1>";
			}
			// định nghĩa phương thức ( thực chất là các hàm bên trong class)
			// bằng cách dùng từ khóa public, protected, private+ function + tenham()

			public function thongtin(){

				// muốn sử dụng biến ở bên trong hàm , phải khai báo biến đó thành biến toàn cục bằng từ khóa global
				global $str;
				echo $str;
				// Để tác động vào một biếng (thuộc tính) của class, sử dụng cú pháp $this-> tenbien

				echo "<h1>Màu sắc: " .$this->mausac."</h1>";
				echo "<h1>Thương hiệu: ".$this->thuonghieu."</h1>";
			}
		}

		// Để sử dụng class , cần khai báo một đối tượng của class đó

		$xh = new xehoi();

		$xh->thongtin();
		// Để tác động vào một hàm bên trong class sử dụng cấu trúc:  tendoituong -> tenham();
	?>
</body>
</html>