<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">
<!-- 
	- Kết thừa: class con kế thừa class cha theo cú pháp: class con extends cha. Khi đó class con sẽ được sử dụng các hàm, các biến của class cha


	- Tầm vực của biến: public, protected, private.
		-public: có thể tác động được
			- từ bên ngoài class
			- từ bên trong class
			- class nó được kế thừa

		- protected: có thể tác động được
			- bên trong chính class đó
			- class nó được kế thừa
			- bên ngoài không tác động được vào bên trong class

		- private: 
			- bên trong chính class đó
			- class được kế thừa không tác động được
			- bên ngoài không tác động được vào bên trong class

 -->
</head>
<body>

	<?php

	class cha{
		
		public function str_cha(){
			echo "<h1>Cha : Hello world</h1>";
		}

		public function str(){
			echo "<h1> Cha : test</h1>";
		}

	}

	class con extends cha{
		public function str_con(){
			echo "<h1>Con :  Hello world</h1>";
			$this->str_cha();
		}
		

		public function str(){
			echo "<h1> COn : test</h1>";
			// gọi lên hàm của class cha (khi các hàm trùng tên nhau), sử dụng từ khóa parent::tenham()
			parent::str();
		}

	}

	// Khởi tạo object của class con
	$c = new con();
	$c-> str_con();
	$c-> str();
	// nếu không sử dụng từ khóa parent thì str ưu tiên gọi đến hàm con, 


	?>
</body>
</html>