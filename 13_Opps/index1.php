<!DOCTYPE html>
<html>
<head>
	<title>OOP</title>
	<meta charset="utf-8">
<!-- 
	
 -->
</head>
<body>

	<?php
//Muốn sử dụng session phải start_session(), khi không start thì nó chỉ là biến cục bộ, và không tồn tại trên tất cả các tab của trình duyệt

	session_start();
	// kiểm tra, nếu session sv chưa tồn tại thì khởi tạo
	if(isset($_SESSION["sv"])==false)
		$_SESSION["sv"]= array();
	// tạo class sinhvien
	class sinhvien{
		//khai báo hàm tạo
		public function __construct(){
			// kiem tra , nếu trang ở trạng thái post thì thực hiện thêm thông tin vào session array
			if($_SERVER['REQUEST_METHOD']=="POST"){
				$hoten =$_POST["hoten"];
				$email =$_POST["email"];
				//gọi hàm thêm để thêm phần tử vào array
				$this->them($hoten,$email);
				$this->xoa($hoten,$email);
			}
			//gọi hàm hiển thị thông tin sinh viên
			$this->hienthi();
			
		}

		// hàm thêm sinh viên
		public function them($hoten, $email){
			$_SESSION["sv"][]= array("hoten"=>$hoten, "email"=>$email);
			// khi bấn f5 thì hàm tự động thêm 1 cái trước
			header("location:index.php");
			// quay lại header để không tự động thêm

		}
		
		//hàm thiển thị thông tin sinh viên
		public function hienthi(){
			//load file sv1.php
			include "sv1.php";
		}

	}
	//nếu muốn sử dụng các hàm trong sinhvien thì $a= new sinhvien();
	// còn không nếu chỉ cần sinhvien thực thi các hàm có thể không cần $a
	new sinhvien();
	?>
</body>
</html>